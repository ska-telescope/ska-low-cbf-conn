from scapy.all import *


class PSR(Packet):
    name = "PSR Packet"
    fields_desc = [
        LongField("sequence_number", 5),
        LongField("timestamp", 5),
        IntField("timestamp_from_epoch", 3),
        IntField("channel_separation", 3),
        LongField("first_channel_freq", 8),
        IntField("scale_1", 3),
        IntField("scale_2", 3),
        IntField("scale_3", 3),
        IntField("scale_4", 3),
        IntField("first_channel_number", 3),
        ShortField("channels_per_packet", 1),
        ShortField("valid_channels_per_packet", 1),
        ShortField("no_time_samples", 1),
        ShortField("beam_number", 1),
        IntField("magic_word", 3),
        XByteField("pkt_dst", 1),
        XByteField("data_precs", 1),
        XByteField("avg_power", 1),
        XByteField("ts_per_rel_wt", 1),
        XByteField("os_num", 1),
        XByteField("os_denom", 1),
        ShortField("beamformer_version", 1),
        LongField("scan_id", 5),
        IntField("offset_1", 3),
        IntField("offset_2", 3),
        IntField("offset_3", 3),
        IntField("offset_4", 3),
        StrLenField("data", "A" * 8000),
    ]
