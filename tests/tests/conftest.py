import pytest
import tango
from async_packet_test.context import TestContext
from tango import Database, DeviceProxy
from tango.test_utils import DeviceTestContext

from ska_low_cbf_conn.low_cbf_connector import LowCbfConnector
from ska_low_cbf_conn.spead_connector import SpeadConnector

from .allocator import Allocator


# pylint: disable=unused-argument
def pytest_sessionstart(session):
    """
    Pytest hook; prints info about tango version.
    :param session: a pytest Session object
    :type session: :py:class:`pytest.Session`
    """
    print(tango.utils.info())


def pytest_addoption(parser):
    """
    Pytest hook; implemented to add the `--true-context` option, used to
    indicate that a true Tango subsystem is available, so there is no
    need for a :py:class:`tango.test_context.MultiDeviceTestContext`.
    :param parser: the command line options parser
    :type parser: :py:class:`argparse.ArgumentParser`
    """
    parser.addoption(
        "--true-context",
        action="store_true",
        default=False,
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )


@pytest.fixture(scope="class")
def device_proxy():
    dp = DeviceProxy("low-cbf/connector/0")
    # FIXME - disabled because re-init of device breaks something
    #  probably best to address when updating to latest base classes
    # dp.Init()
    return dp


@pytest.fixture(scope="class")
def test_context():
    with DeviceTestContext(LowCbfConnector, process=True) as test_context:
        yield test_context


@pytest.fixture(scope="module")
def context():
    ctx = TestContext()
    return ctx


@pytest.fixture(scope="module")
def ctrl():
    ctrl = SpeadConnector("tna_telemetry", "127.0.0.1", "50052", 0)
    yield ctrl
    # TODO check if these can be moved here?
    # ctrl.clear_simple_table()
    # ctrl.clear_spead_table()


@pytest.fixture(scope="module")
def allocator():
    allocator = Allocator("./tests/tests/allocator_output.txt")
    yield allocator
    # TODO check if these can be moved here?
    # ctrl.clear_simple_table()
    # ctrl.clear_spead_table()
