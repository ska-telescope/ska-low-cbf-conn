from scapy.all import *


class Spead(Packet):
    name = "SPEAD Packet"
    fields_desc = [
        LongField("spead_header", 8),  # 8bytes
        IntField("high_heap_counter", 3),  # 4bytes
        IntField("high_heap", 3),  # 4bytes
        LongField("spead_payload_len", 8),
        LongField("ref_time", 5),
        LongField("frame_timestamp", 5),
        LongField("frew_channel", 5),
        IntField("pad", 3),  # 4bytes
        ShortField("beam_no", 1),  # 2bytes
        ShortField("frequency_no", 1),
        X3BytesField("pad2", 1),
        XByteField("sub_array", 1),
        IntField("station_no", 3),
        IntField("num_antennas", 3),
        StrLenField("data", "A" * 8000),
    ]


class SpeadInitialisation(Packet):
    name = "SPEAD Initialisation Packet LOW CBF to SDP"
    fields_desc = [
        XByteField("magic", 83),  # 0x53
        XByteField("version", 4),  # 0x04
        XByteField("ItemPointerWidth", 2),  # 0x02
        XByteField("HeapAddWidth", 6),  # 0x06
        ShortField("Reserved", 0),  # 0x00
        ShortField("NumberOfItems", 20),  # 0x0F
        ShortField("HeapCounteHdr", 32769),  # 0x8001
        IntField("SubArray_Beam_Frequency", 1),
        ShortField("integrationID", 0),
        ShortField("HeapSizeHdr", 32770),  # 0x8002
        ShortField("reserved_size", 0),
        IntField("HeapSize", 864),
        ShortField("HeapOffsetHdr", 32771),  # 0x8003
        ShortField("reserved_offset", 0),
        IntField("OffsetSize", 0),
        ShortField("HeapPayloadHdr", 32772),  # 0x8004
        ShortField("reserved_payload", 0),
        IntField("PayloadSize", 864),
        ShortField("StreamControlHdr", 32774),  # 0x8006
        ShortField("reserved_control", 0),
        IntField("StreamControl", 0),
        ### Now we add all the Item descriptor Items in group of 3 Fields
        ### fields 1 and 2 are standard
        ### field 3 should contain the first byte of the payload (0) and then each subsequent field
        ### 3 increment by the size of each spead packet
        ShortField("ItemDescriptorTimeS", 5),  # 0x0005
        ShortField("reserved_descriptor_TimeS", 0),
        IntField("Address_of_pointer_TimeS", 0),  # TO be computed
        ShortField("ItemDescriptorTimeF", 5),  # 0x0005
        ShortField("reserved_descriptor_TimeF", 0),
        IntField("Address_of_pointer_TimeF", 72),  # 0x48
        ShortField("ItemDescriptorSubarray", 5),  # 0x0005
        ShortField("reserved_descriptor_Subarray", 0),
        IntField("Address_of_pointer_Subarray", 144),  # 0x48
        ShortField("ItemDescriptorChannel", 5),  # 0x0005
        ShortField("reserved_descriptor_Channel", 0),
        IntField("Address_of_pointer_Channel", 216),  # 0x48
        ShortField("ItemDescriptorScan", 5),  # 0x0005
        ShortField("reserved_descriptor_Scan", 0),
        IntField("Address_of_pointer_Scan", 288),  # 0x48
        ShortField("ItemDescriptorBeam", 5),  # 0x0005
        ShortField("reserved_descriptor_Beam", 0),
        IntField("Address_of_pointer_Beam", 360),  # 0x48
        ShortField("ItemDescriptorBaseline", 5),  # 0x0005
        ShortField("reserved_descriptor_Baseline", 0),
        IntField("Address_of_pointer_Baseline", 432),  # 0x48
        ShortField("ItemDescriptorHardware", 5),  # 0x0005
        ShortField("reserved_descriptor_Hardware", 0),
        IntField("Address_of_pointer_Hardware", 504),  # 0x48
        ShortField("ItemDescriptorIntegration", 5),  # 0x0005
        ShortField("reserved_descriptor_Integration", 0),
        IntField("Address_of_pointer_Integration", 576),  # 0x48
        ShortField("ItemDescriptorFrequency", 5),  # 0x0005
        ShortField("reserved_descriptor_Frequency", 0),
        IntField("Address_of_pointer_Frequency", 648),  # 0x48
        ShortField("ItemDescriptorResolution", 5),  # 0x0005
        ShortField("reserved_descriptor_Resolution", 0),
        IntField("Address_of_pointer_Resolution", 720),  # 0x48
        ShortField("ItemDescriptorCorrelation", 5),  # 0x0005
        ShortField("reserved_descriptor_Correlation", 0),
        IntField("Address_of_pointer_Correlation", 792),  # 0x48
        ShortField("SubarrayHdr", 57346),  # 0xE002
        ShortField("reserved_Subarray", 0),
        IntField("Subarray", 1),  # 0x48
        ShortField("ChannelHdr", 57347),  # 0xE003
        ShortField("reserved_Channel", 0),
        IntField("Channel", 1),  # 0x48
        ShortField("BeamHdr", 57349),  # 0xE005
        ShortField("reserved_Beam", 0),
        IntField("Beam", 1),  # 0x48
        ### Static values for the stream
    ]


class SpeadDescriptor(Packet):
    name = "SPEAD Initialisation Packet LOW CBF to SDP"
    fields_desc = [
        XByteField("magic", 83),  # 0x53
        XByteField("version", 4),  # 0x04
        XByteField("ItemPointerWidth", 2),  # 0x02
        XByteField("HeapAddWidth", 6),  # 0x06
        ShortField("Reserved", 0),  # 0x00
        ShortField("NumberOfItems", 7),  # 0x0F
        ShortField("HeapCounteHdr", 32769),  # 0x8001
        IntField("SubArray_Beam_Frequency", 0),
        ShortField("integrationID", 1),
        ShortField("HeapSizeHdr", 32770),  # 0x8002
        ShortField("reserved_size", 0),
        IntField("HeapSize", 8),
        ShortField("HeapOffsetHdr", 32771),  # 0x8003
        ShortField("reserved_offset", 0),
        IntField("OffsetSize", 0),
        ShortField("HeapPayloadHdr", 32772),  # 0x8004
        ShortField("reserved_payload", 0),
        IntField("PayloadSize", 8),
        ShortField("IdentifierHdr", 32788),  # 0x8014
        ShortField("reserved_identifier", 0),
        IntField("Identifier", 1),  # New identifier header
        ShortField("NameHdr", 16),  # 0x0010
        ShortField("reserved_name", 0),
        IntField("PointerToName", 0),  # New identifier header
        ShortField("TypeHdr", 19),  # 0x0013
        ShortField("reserved_type", 0),
        IntField("PointerToType", 0),  # pointer
    ]  # size = 64 bytes


class SpeadSDP(Packet):
    name = "SPEAD Initialisation Packet LOW CBF to SDP"
    fields_desc = [
        XByteField("magic", 83),  # 0x53
        XByteField("version", 4),  # 0x04
        XByteField("ItemPointerWidth", 2),  # 0x02
        XByteField("HeapAddWidth", 6),  # 0x06
        ShortField("Reserved", 0),  # 0x00
        ShortField("NumberOfItems", 7),  # 0x0F
        ShortField("HeapCounteHdr", 32769),  # 0x8001
        IntField("SubArray_Beam_Frequency", 0),
        ShortField("integrationID", 1),
        ShortField("HeapSizeHdr", 32770),  # 0x8002
        ShortField("reserved_size", 0),
        IntField("HeapSize", 8),
        ShortField("HeapOffsetHdr", 32771),  # 0x8003
        ShortField("reserved_offset", 0),
        IntField("OffsetSize", 0),
        ShortField("HeapPayloadHdr", 32772),  # 0x8004
        ShortField("reserved_payload", 0),
        IntField("PayloadSize", 8),
        ShortField("TimeSHdr", 57344),  # 0xE000
        ShortField("reserved_TimeS", 0),
        IntField("TimeS", 1),  # 0x48
        ShortField("TimeFHdr", 57345),  # 0xE001
        ShortField("reserved_TimeF", 0),
        IntField("TimeF", 1),  # 0x48
        ShortField("SubarrayHdr", 57346),  # 0xE002
        ShortField("reserved_Subarray", 0),
        IntField("Subarray", 1),  # 0x48
        ShortField("ChannelHdr", 57347),  # 0xE003
        ShortField("reserved_Channel", 0),
        IntField("Channel", 1),  # 0x48
        ShortField("BeamHdr", 57349),  # 0xE005
        ShortField("reserved_Beam", 0),
        IntField("Beam", 1),  # 0x48
        ShortField("CorrelationHdr", 24587),  # 0x600C
        ShortField("reserved_Correlation", 0),
        IntField("Correlation", 0),  # 0x48
    ]
