"""
Tests for Low CBF Connector Tango Device
"""

import json
from unittest.mock import PropertyMock, patch

import pytest
from tango.test_utils import DeviceTestContext

from ska_low_cbf_conn.low_cbf_connector import LowCbfConnector


class TestContext:
    @pytest.mark.parametrize(
        "attr",
        ["routeCounters", "dynamicRoutes", "routingTable", "staticConfig"],
    )
    def test_attributes(self, attr, test_context):
        """Check that expected attributes exist & readable"""
        with DeviceTestContext(LowCbfConnector, process=True) as dtc:
            assert attr in dtc.get_attribute_list()
            getattr(dtc, attr)


class TestMockConnector:
    @pytest.mark.parametrize(
        "connector_property, tango_attribute",
        [
            # These attributes do some processing, so our basic Mock test will
            # fail.
            # ("counter_unicast", "speadUnicastCounterTotal"),
            # ("counter_multiplier", "speadMultiplierCounterTotal"),
            ("lost_spead_bytes", "bytesLost"),
            ("lost_spead_packets", "packetsLost"),
            ("loss_rate_spead_bytes", "bytesLossRate"),
            ("loss_rate_spead_packets", "packetLossRate"),
        ],
    )
    def test_json_wrap(self, connector_property, tango_attribute):
        """Tests Tango attributes which return JSON derived from Connector properties"""
        test_data = {1: {"TYPE1": 10, "TYPE2": 20}}
        with patch(
            "ska_low_cbf_conn.low_cbf_connector.SpeadConnector"
        ) as mock_connector:
            setattr(
                type(mock_connector.return_value),
                connector_property,
                PropertyMock(return_value=test_data),
            )
            with DeviceTestContext(LowCbfConnector, process=True) as dtc:
                assert getattr(dtc, tango_attribute) == json.dumps(test_data)


# probably delete this?
@pytest.mark.post_deployment
@pytest.mark.usefixtures("device_proxy")
class TestProxy:
    def test_proxy_exists(self, device_proxy):
        assert device_proxy is not None


@pytest.mark.post_deployment
@pytest.mark.usefixtures("device_proxy")
class TestWithGrpc:
    def test_add_route(self, device_proxy):
        """Add a packet forwarding rule"""
        device_proxy.AddSpeadUnicastEntry(
            '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port": "12/0"}}]}'
        )

    def test_del_route(self, device_proxy):
        """Delete a packet forwarding rule"""
        device_proxy.ClearSpeadUnicastTable("all")

    def test_add_routes(self, device_proxy):
        """Add multiple packet forwarding rules"""
        device_proxy.AddSpeadUnicastEntry(
            '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port": "12/0"}}, {"src": {"frequency": 123, "beam": 23, "sub_array": 1 }, "dst": {"port": "12/0"}}]}'
        )

    def test_del_routes(self, device_proxy):
        """Add multiple packet forwarding rules"""
        device_proxy.ClearSpeadUnicastTable("all")
