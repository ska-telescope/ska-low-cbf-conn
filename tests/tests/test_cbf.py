import re

import pytest
from async_packet_test.predicates import (
    Predicate,
    did_not_see_packet_equaling,
    saw_packet_equaling,
)
from scapy.all import IP, UDP, Ether, sendp

from .psr_packet import PSR
from .spead_packet import Spead

front_panel_regex = re.compile("([0-9]+)/([0-9]+)$")

packet = (
    Ether()
    / IP()
    / UDP(sport=4550, dport=4660)
    / Spead(frequency_no=1, beam_no=2, sub_array=3)
)
packet_update = (
    Ether()
    / IP()
    / UDP(sport=4551, dport=4660)
    / Spead(frequency_no=1, beam_no=2, sub_array=3)
)
packet2 = (
    Ether()
    / IP()
    / UDP(sport=4550, dport=4660)
    / Spead(frequency_no=2, beam_no=2, sub_array=3)
)
packet_psr = (
    Ether() / IP() / UDP(sport=4550, dport=9510) / PSR(beam_number=256)
)
packet_psr_update = (
    Ether() / IP() / UDP(sport=4551, dport=9510) / PSR(beam_number=256)
)
packet_psr_2 = (
    Ether() / IP() / UDP(sport=4550, dport=9510) / PSR(beam_number=512)
)


# Note these tests fail unless you have a real barefoot runtime
# (and P4 switch?)
@pytest.mark.bfrt
@pytest.mark.usefixtures("context", "ctrl")
class TestBeamformer:
    def test_saw_packet_spead_table(self, context, ctrl):

        result = context.expect("veth0", saw_packet_equaling(packet))
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.add_spead_table_entry(1, 2, 3, front_panel_regex.match("1/0"))
        success, table = ctrl.get_spead_table_entries()
        expected_table = []
        expected_table.append(
            {"Frequency": "1", "Beam": "2", "Sub_array": "3", "port": "1/0"}
        )
        assert table == expected_table
        sendp(packet, iface="veth2")
        result.assert_true()
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()

    def test_did_not_see_packet_spead_table(self, context, ctrl):
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        result = context.expect("veth4", did_not_see_packet_equaling(packet2))
        # result1 = context.expect("veth6", did_not_see_packet_equaling(packet2))
        result2 = context.expect("veth8", did_not_see_packet_equaling(packet2))
        result3 = context.expect(
            "veth10", did_not_see_packet_equaling(packet2)
        )
        result4 = context.expect(
            "veth12", did_not_see_packet_equaling(packet2)
        )
        result5 = context.expect(
            "veth14", did_not_see_packet_equaling(packet2)
        )

        ctrl.add_spead_table_entry(2, 2, 3, front_panel_regex.match("1/0"))
        sendp(packet2, iface="veth2")

        assert result
        # assert result1
        assert result2
        assert result3
        assert result4
        assert result5
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()

    def test_saw_packet_and_update_spead_table(self, context, ctrl):

        result = context.expect("veth0", saw_packet_equaling(packet))
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.add_spead_table_entry(1, 2, 3, front_panel_regex.match("1/0"))
        sendp(packet, iface="veth2")
        result.assert_true()
        result_update = context.expect(
            "veth4", saw_packet_equaling(packet_update)
        )
        ctrl.update_spead_table_entry(1, 2, 3, front_panel_regex.match("1/2"))
        sendp(packet_update, iface="veth2")
        result_update.assert_true()
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()

    def test_spead_multiplier_counters_table(self, context, ctrl):
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()
        success, pre_counters = ctrl.get_spead_table_counters()
        assert len(pre_counters) == 0
        ctrl.add_spead_table_entry(
            1,
            2,
            3,
            front_panel_regex.match("1/0"),
        )
        sendp(packet, iface="veth4")
        post_result = []
        post_result.append(
            {
                "Frequency": "1",
                "Beam": "2",
                "Sub_array": "3",
                "Pkts": "1",
                "Bytes": "8114",
            }
        )
        success, post_counters = ctrl.get_spead_table_counters()
        assert post_counters == post_result
        ctrl.reset_spead_counters()
        reset_result = []
        reset_result.append(
            {
                "Frequency": "1",
                "Beam": "2",
                "Sub_array": "3",
                "Pkts": "0",
                "Bytes": "0",
            }
        )
        success, reset_counters = ctrl.get_spead_table_counters()
        assert reset_counters == reset_result
        ctrl.reset_psr_counters()
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()


@pytest.mark.bfrt
@pytest.mark.usefixtures("context", "ctrl", "allocator")
class TestSpeadmultiplier:
    def test_saw_packet_spead_multiplier_table(self, context, ctrl):

        result = context.expect("veth0", saw_packet_equaling(packet))
        result1 = context.expect("veth2", saw_packet_equaling(packet))
        ctrl.clear_spead_table()
        ctrl.clear_simple_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.add_spead_multiplier_table_entry(
            1,
            2,
            3,
            front_panel_regex.match("1/0"),
            front_panel_regex.match("1/1"),
        )
        success, table = ctrl.get_spead_multiplier_table_entries()
        expected_table = []
        expected_table.append(
            {"Frequency": "1", "Beam": "2", "Sub_array": "3", "session": "1"}
        )
        assert table == expected_table
        sendp(packet, iface="veth4")
        result.assert_true()
        result1.assert_true()
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()

    def test_did_not_see_packet_spead_multiplier_table(self, context, ctrl):

        results = [
            context.expect("veth6", did_not_see_packet_equaling(packet)),
            context.expect("veth8", did_not_see_packet_equaling(packet)),
            context.expect("veth10", did_not_see_packet_equaling(packet)),
            context.expect("veth12", did_not_see_packet_equaling(packet)),
            context.expect("veth14", did_not_see_packet_equaling(packet)),
        ]
        ctrl.clear_spead_table()
        ctrl.clear_simple_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.add_spead_multiplier_table_entry(
            1,
            2,
            3,
            front_panel_regex.match("1/0"),
            front_panel_regex.match("1/1"),
        )
        sendp(packet, iface="veth4")

        for result in results:
            assert result

        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()

    def test_saw_packet_and_update_spead_multiplier_table(self, context, ctrl):

        result = context.expect("veth0", saw_packet_equaling(packet))
        result1 = context.expect("veth2", saw_packet_equaling(packet))
        ctrl.clear_spead_table()
        ctrl.clear_simple_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.add_spead_multiplier_table_entry(
            1,
            2,
            3,
            front_panel_regex.match("1/0"),
            front_panel_regex.match("1/1"),
        )
        sendp(packet, iface="veth4")
        result.assert_true()
        result1.assert_true()
        result_update = context.expect(
            "veth0", saw_packet_equaling(packet_update)
        )
        result_update1 = context.expect(
            "veth6", saw_packet_equaling(packet_update)
        )
        ctrl.update_spead_multiplier_table_entry(
            1,
            2,
            3,
            front_panel_regex.match("1/0"),
            front_panel_regex.match("1/3"),
        )
        sendp(packet_update, iface="veth4")
        result_update.assert_true()
        result_update1.assert_true()
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()

    def test_allocator_input(self, context, ctrl, allocator):
        configurations = allocator.get_list_of_cbf()
        ctrl.clear_spead_table()
        ctrl.clear_simple_table()
        ctrl.clear_spead_multiplier_table()
        packets_to_send = []
        results = []
        for configuration in configurations:
            packet_allocator = (
                Ether()
                / IP()
                / UDP(sport=4550, dport=4660)
                / Spead(
                    frequency_no=configuration[0],
                    beam_no=configuration[1],
                    sub_array=configuration[2],
                )
            )
            packets_to_send.append(packet_allocator)
            ctrl.add_spead_multiplier_table_entry(
                configuration[0],
                configuration[1],
                configuration[2],
                front_panel_regex.match(configuration[3]),
                front_panel_regex.match(configuration[4]),
            )
            results.append(
                context.expect(
                    allocator.get_veth_from_port(configuration[3]),
                    saw_packet_equaling(packet_allocator),
                )
            )
            results.append(
                context.expect(
                    allocator.get_veth_from_port(configuration[4]),
                    saw_packet_equaling(packet_allocator),
                )
            )
        for packets in packets_to_send:
            sendp(packets, iface="veth4")
        for result in results:
            assert result

    def test_spead_multiplier_counters_table(self, context, ctrl):
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()
        success, pre_counters = ctrl.get_spead_multiplier_table_counters()
        assert len(pre_counters) == 0
        ctrl.add_spead_multiplier_table_entry(
            1,
            2,
            3,
            front_panel_regex.match("1/0"),
            front_panel_regex.match("1/1"),
        )
        sendp(packet, iface="veth4")
        post_result = []
        post_result.append(
            {
                "Frequency": "1",
                "Beam": "2",
                "Sub_array": "3",
                "Pkts": "1",
                "Bytes": "8114",
            }
        )
        success, post_counters = ctrl.get_spead_multiplier_table_counters()
        assert post_counters == post_result
        ctrl.reset_spead_multiplier_counters()
        reset_result = []
        reset_result.append(
            {
                "Frequency": "1",
                "Beam": "2",
                "Sub_array": "3",
                "Pkts": "0",
                "Bytes": "0",
            }
        )
        success, reset_counters = ctrl.get_spead_multiplier_table_counters()
        assert reset_counters == reset_result
        ctrl.reset_psr_counters()
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()


@pytest.mark.bfrt
@pytest.mark.usefixtures("context", "ctrl")
class TestPSR:
    def test_saw_packet_psr_table(self, context, ctrl):

        result = context.expect("veth0", saw_packet_equaling(packet_psr))
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()
        ctrl.add_psr_table_entry(1, front_panel_regex.match("1/0"))
        success, table = ctrl.get_psr_table_entries()
        expected_table = []
        expected_table.append({"Beam": "1", "port": "1/0"})
        assert table == expected_table
        sendp(packet_psr, iface="veth2")
        result.assert_true()
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()

    def test_did_not_see_packet_psr_table(self, context, ctrl):

        results = [
            context.expect("veth6", did_not_see_packet_equaling(packet_psr)),
            context.expect("veth8", did_not_see_packet_equaling(packet_psr)),
            context.expect("veth10", did_not_see_packet_equaling(packet_psr)),
            context.expect("veth12", did_not_see_packet_equaling(packet_psr)),
            context.expect("veth14", did_not_see_packet_equaling(packet_psr)),
        ]
        ctrl.clear_spead_table()
        ctrl.clear_simple_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_psr_table()
        ctrl.add_psr_table_entry(1, front_panel_regex.match("1/0"))
        sendp(packet_psr, iface="veth4")

        for result in results:
            assert result

        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()

    def test_saw_packet_and_update_psr_table(self, context, ctrl):

        result = context.expect("veth0", saw_packet_equaling(packet_psr))
        ctrl.clear_spead_table()
        ctrl.clear_simple_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_psr_table()
        ctrl.add_psr_table_entry(1, front_panel_regex.match("1/0"))
        sendp(packet_psr, iface="veth4")
        result.assert_true()

        result_update = context.expect(
            "veth6", saw_packet_equaling(packet_psr_update)
        )
        ctrl.update_psr_table_entry(
            1,
            front_panel_regex.match("1/3"),
        )
        sendp(packet_psr_update, iface="veth4")
        result_update.assert_true()

        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()

    def test_psr_counters_table(self, context, ctrl):
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()
        success, pre_counters = ctrl.get_psr_table_counters()
        assert len(pre_counters) == 0
        ctrl.add_psr_table_entry(1, front_panel_regex.match("1/0"))
        sendp(packet_psr, iface="veth2")
        post_result = []
        post_result.append({"Beam": "1", "Pkts": "1", "Bytes": "8142"})
        success, post_counters = ctrl.get_psr_table_counters()
        assert post_counters == post_result

        ctrl.reset_psr_counters()
        ctrl.clear_spead_table()
        ctrl.clear_spead_multiplier_table()
        ctrl.clear_simple_table()
        ctrl.clear_psr_table()
