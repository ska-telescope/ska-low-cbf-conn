# Changelog

### 0.7.2
* upgrade base images and pytango
  * ska-build-python 0.1.1 
  * ska-tango-images-tango-python 0.2.0
  * pytango 10.0.0 
* fixes memory leak for SKB-693

### 0.7.1
* port monitor checking if qSFP present before requesting information (fixing SKB-727)

### 0.7.0
* More than 2 destinations enable for SPS routing 
* PSR routing now changes UDP destination port (requires ska-low-cbf-p4 0.6.0 on switch)

### 0.6.3
* Changed classic attribute style (`my_attr = attribute( dtype=...)`) with decorated style (`@attribute`) in order to assist with Sphinx (autodoc) docstring extraction for Read-the-Docs pages
* Fix memory leak from scapy in ARP resolver

### 0.6.2
* Update to pytango 9.5, ska-tango-base v1.1, ska-control-model v1.0

### 0.6.1
* usage of Intel gRPC instead of home made gRPC server to retrieve various sensor readings

### 0.6.0
* 3 new attributes for health and associated configuration yaml file
  * hardware health: power and temperature at the moment
  * function health: port operational, code, then later feasibility of routing
  * process health: data rate and later queue occupancy
* helm chart incorporating port type of service
* attributes per service indicating the percentage of port available
* Updated documentation for the health
* Latest taranta in the test deployment
* 291 new attributes for diagnostics
  * serial number of the swich
  * average temperature from the motherboard
  * tofino board temperature
  * 9 attributes for each of the 32 ports
    * temperature of QSFP 
    * serial number of QSFP
    * Tx power per lane 
    * Rx power per lane
    * Up/Down status
    * Received Packets Per second
    * Received Bytes per second
    * Transmitted Packets Per second
    * Transmitted Bytes per second
* gRPC server, client, proto file, and generated files for manipulating BF shell 

### 0.5.5
* Alarm handler chart added in helm
* New method to add manually port mapping to be used for Tofino CPU port in particular

### 0.5.4
* advanced telemetry enabled
* new helm properties for telemetry

### 0.5.3
* bug fix in the connector to allocator initialisation
* New variables in the helm charts
* Unsubscription from the allocator

### 0.5.2 
* Point to correct image in Helm chart

### 0.5.1
* EDA configuration file 
* extending connector telemetry with health status
* new read the docs theme
* inline hostPath for kubernetes install

### 0.5.0
* SDP routing using IP address and port
* New telemetry using SDE 9.13.0 (rate and packet per second from ASIC)
* Tango Health Status variable containing rate and PPS in addition to byte, packet received per port
* Tango Health State is set to DEGRADED is one port no longer up and active
* Tango Device automatic configuration via registration to Allocator internal variables
* ARP support for SDP network
* Support for substation manipulation (only used in debugging mode, not production)
* PTP configuration support

### 0.4.14
* Tango methods to control the various table extended and made more modular with the deletion of rule
* Removed NetworkAttachmentDefinition template - not required, NAD in default
namespace is used by values.yaml anyway

### 0.4.12
* Configuration of the physical ports is now fixed

### 0.4.11
* Call Back function with Allocator now cleans up obsolete routes

### 0.4.10
* Update Helm chart to use new volume mounting feature of ska-tango-util,
rather than our previous local fork

### 0.4.9
* Integration of Tango connector with Low.CBF Allocator

### 0.4.8
* ARP traffic can be received by SDP pods in Low PSI 

### 0.4.7
* New class to implement the ARP resolution 

### 0.4.6
* Advanced Telemetry is available
* Tango connector does not connect directly to the switch\_d daemon
* Preliminary CI tests running on Low PSI (unit tests disabled until we can set
up a runner on another P4 swtich)

### 0.4.5
* Tests for the various table defined in the connector have been implemented
* Tests for the tango connector remained minimal waiting for test environment

### 0.4.4
* Tango can now configure the various table of the switch

### 0.4.2
* Beamformer and correlator tests are now also configured with allocator output

### 0.4.1
* Simple tests for correlator and bearmformer are available.
* Fixed update and adding functions of the correlator

### 0.4.0
* Correlator can now be configured in parallel to the Beamformer

### 0.3.5
* Tests are running in the CI pipeline

### 0.3.4
* Tests includes SPEAD tests, Simple table tests, ingress/egress counters

### 0.3.3
* Split from [ska-low-cbf-net](https://gitlab.com/ska-telescope/ska-low-cbf-net)
* Create SpeadConnector & SpeadCli classes that add PSR & SPEAD protocol
functionality to base Connector & Cli classes, inherited from ska-low-cbf-net
