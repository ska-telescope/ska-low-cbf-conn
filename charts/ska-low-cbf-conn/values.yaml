enabled: true

telescope: SKA-low
system: SW-infrastructure
subsystem: ska-low-cbf-network

labels:
  app: ska-low-cbf

global:
  minikube: true
  tango_host: databaseds-tango-base-test:10000
  tries: 10
  sleep: 5

dsconfig:
  timeout: 300s
  image:
      registry: artefact.skao.int
      image: ska-tango-images-tango-dsconfig
      tag: 1.5.7
      pullPolicy: IfNotPresent

itango:
  image:
    registry: artefact.skao.int
    image: ska-tango-images-tango-itango
    tag: 9.3.10
    pullPolicy: IfNotPresent

image:
  registry: artefact.skao.int
  image: ska-low-cbf-conn
  tag: ~ # defaults to chart's appVersion when empty
  pullPolicy: IfNotPresent

resources:
  limits:
    cpu: 4
    memory: 2Gi
  requests:
    cpu: 2
    memory: 1Gi

cbf:
  network:
    proxy: ""
  connector_db_host: "tango-databaseds.ska-low-cbf-conn"
  connector_db_port: 10000
  hardware_connections:
    # psi-perentie1
    - 'switch=p4_01  port=19/0  speed=100  alveo=XFL14SLO1LIF'
    - 'switch=p4_01  port=21/0  speed=100  alveo=XFL1DKXBEVG2'

connector:
  bfrt_address: "rand_address:rand_port"
  allocator_address: "rand_url"
  ports_configuration: "do_not_configure_ports"
  switch: "p4_01"
  ebpf_configuration: "no_advanced_telemetry"
  bfrt_shell_address: "rand_address:rand_port"

deviceServers:
  connector:
    instances: ["default"]
    name: "connector-{{.Release.Name}}"
    function: ska-low-cbf-connector
    domain: ska-low-cbf-conn
    command: "LowCbfConnector"
    securityContext:
      privileged: true
    nodeSelector:
      kubernetes.io/hostname: psi-p4
    server:
      name: "LowCbfConnector"
      instances:
        - name: "default"
          classes:
            - name: "LowCbfConnector"
              devices:
                - name: "low-cbf/connector/0"
                  properties:
                    - name: "bfrt_address"
                      values:
                        - "{{ tpl .Values.connector.bfrt_address .}}"
                    - name: "allocator_address"
                      values:
                        - "{{ tpl .Values.connector.allocator_address .}}"
                    - name: "ports_configuration"
                      values:
                        - "{{ tpl .Values.connector.ports_configuration .}}"
                    - name: "switch"
                      values:
                        - "{{ tpl .Values.connector.switch .}}"
                    - name: "ebpf_configuration"
                      values:
                        - "{{ tpl .Values.connector.ebpf_configuration .}}"
                    - name: "bfrt_shell_address"
                      values:
                        - "{{ tpl .Values.connector.bfrt_shell_address .}}"
                    # TODO needs to not be hardcoded
                    - name: "hardware_connections"
                      values:
                        - 'switch=p4_01  port=7/0  speed=100  alveo=XFL1TJCHM3ON'
                        - 'switch=p4_01  port=9/0  speed=100  alveo=XFL1VCYSXCL0'
                        - 'switch=p4_01  port=11/0  speed=100  alveo=XFL10NIYKVEU'
                        - 'switch=p4_01  port=13/0  speed=100  alveo=XFL1XCRTUC22'
                        - 'switch=p4_01  port=15/0  speed=100  alveo=XFL1E35JVJTQ'
                        - 'switch=p4_01  port=17/0  speed=100  alveo=XFL1RCFEG244'
                        - 'switch=p4_01  port=2/0  speed=100  link=stn_000'
                        - 'switch=p4_01  port=5/0  speed=100  link=stn_001'
                        - 'switch=p4_01  port=28/0  speed=100  link=pst_001'
                        - 'switch=p4_01  port=29/0  speed=100  link=pss_001'
                        - 'switch=p4_01  port=32/0  speed=100  link=sdp_001'
                    - name: "polled_attr"
                      values:
                        - "adminMode"
                        - "1000"
                  attribute_properties:
                    - attribute: "adminMode"
                      properties:
                        - name: "abs_change"
                          values:
                            - "-1"
                            - "1"
    depends_on:
      - device: sys/database/2
    image:
      registry: "{{.Values.image.registry}}"
      image: "{{.Values.image.image}}"
      tag: "{{.Values.image.tag | default .Chart.AppVersion}}"
      pullPolicy: "{{.Values.image.pullPolicy}}"
    extraVolumes:
      - name: p4-sde
        hostPath:
          path: /opt/intel/bf-sde
          type: Directory
    extraVolumeMounts:
      - mountPath: /sde
        name: p4-sde
        readOnly: true

affinity: {}

tolerations:
  - key: skao.int/dedicated
    value: low-cbf-p4

annotations:
  k8s.v1.cni.cncf.io/networks: '[{
    "namespace": "default",
    "name": "host-device",
    "deviceID": "0000:04:00.0",
    "ips": ["192.168.1.101/24"]
  }]'


