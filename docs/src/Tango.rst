******************
LowCbfConnector
******************

The LowCbfConnector is a Tango device server for  monitoring and control of registers in the Low.CBF P4 switch.
Tofino-based in-network processors require two compulsory components, the P4 program that will be compiled and run on the switch and the compiler/SDE to compile this code.

In addition, software is required to be able to process runtime commands via a series of API (i.e. gRPC, gNMI), for this part, SKA leverages the Barefoot Runtime gRPCs.
These gRPCs are used being the scene in the Tango devices, through multiple classes centralised in the Connector class, to configure and monitor a given switch.

In terms of Tango device usability, the Connector offers standard operations with:

* attributes representing both states of the tango device itself and the health and monitoring of the P4 switch
* commands to control both tango device and the switch including routing configuration and telemetry.


Tango attribute/command list
############################
.. include:: low_cbf_connector_attr_cmd.rst


Dynamically Created Tango Attributes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
These attributes are created for each P4 switch port (currently ports 1-32) on startup.

.. attribute:: LowCbfConnector.diagnostic_port_N_rxpps
               
   Where N is the port number [1..32]
              
.. attribute:: LowCbfConnector.diagnostic_port_N_serial
               
   Where N is the port number [1..32]

.. attribute:: LowCbfConnector.diagnostic_port_N_txpps
               
   Where N is the port number [1..32]
              
.. attribute:: LowCbfConnector.diagnostic_port_N_up
               
   Where N is the port number [1..32]
              
.. attribute:: LowCbfConnector.hardware_port_N_rxpower
               
   Where N is the port number [1..32]
              
.. attribute:: LowCbfConnector.hardware_port_N_temperature
               
   Where N is the port number [1..32]
              
.. attribute:: LowCbfConnector.hardware_port_N_txpower
               
   Where N is the port number [1..32]
              
.. attribute:: LowCbfConnector.process_port_N_rxthroughput
               
   Where N is the port number [1..32]
   
.. attribute:: LowCbfConnector.process_port_N_txthroughput
               
   Where N is the port number [1..32]
              
