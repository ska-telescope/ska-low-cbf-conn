Low CBF Connector Tango attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: low_cbf_connector::LowCbfConnector.allocatorAddress
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.arpRoutingTable
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.arp_replies
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.basicRoutingTable
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.bfrtAddress
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.byteRate
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.bytesLossRate
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.bytesLost
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.dynamicRoutes
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.function_alveo_port_up
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.function_p4_code_version_present
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.function_pss_port_up
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.function_pst_port_up
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.function_sdp_port_up
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.function_sps_port_up
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.function_valid_routing_port
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.hardware_motherboard_temperature
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.hardware_tofino_temperature
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.healthState
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.health_function
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.health_hardware
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.health_process
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.health_status
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ipAddressToMacAndPort
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.multicastSessions
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.packetLossRate
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.packetRate
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.packetsLost
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.portStatus
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.port_rx_pps
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.port_rx_throughput
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.port_tx_pps
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.port_tx_throughput
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.psrRoutingTable
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.routeCounters
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.sdpIpRoutingTable
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.sdpMacRoutingTable
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.speadMultiplierCounterTotal
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.speadMultiplierRoutingTable
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.speadUnicastCounterTotal
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.speadUnicastRoutingTable
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.spsRates
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.staticConfig
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.subStationTable
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.switchName
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.test_mode_overrides
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.unit_serial
   :noindex:

Low CBF Connector Tango commands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: low_cbf_connector::LowCbfConnector.AddARPEntry()
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddBasicEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddIPToResolve 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddMultiplierUnicastEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddPSREntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddPTPClockPort 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddPTPEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddPortMapping 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddPortsToPTP 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddPortsToSDPARP 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddSDPIPEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddSDPMACEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddSpeadUnicastEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.AddSubStationEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearARPTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearBasicTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearPSRTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearPTPTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearSDPIPTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearSDPMACTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearSpeadMultiplierTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearSpeadUnicastTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ClearSubStationTable 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ConnectToSwitch 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.LoadPorts 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RegisterCallbackAllocator 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemoveARPEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemoveBasicEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemovePSREntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemovePorts 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemoveSDPIPEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemoveSDPMACEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemoveSpeadMultiplierEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemoveSpeadUnicastEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.RemoveSubStationEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.ResetPortStatistics 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UnsubscribeAllocator 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UpdateARPEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UpdateBasicEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UpdateMultiplierUnicastEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UpdatePSREntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UpdatePTPEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UpdateSDPIPEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UpdateSDPMACEntry 
   :noindex:
.. automethod:: low_cbf_connector::LowCbfConnector.UpdateSpeadUnicastEntry
   :noindex:
