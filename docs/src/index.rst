*********************
ska-low-cbf-conn
*********************

Monitoring and control of the Low PSI P4 switch.

.. toctree::
   :maxdepth: 2
   :caption: Readme

   README.md

.. toctree::
   :maxdepth: 2
   :caption: P4 Overview in AA0.5

   Overview

.. toctree::
   :maxdepth: 2
   :caption: Tango Device

   Tango

.. toctree::
   :maxdepth: 2
   :caption: Interfacing with SPS

   SPS

.. toctree::
   :maxdepth: 2
   :caption: Interfacing with SDP

   SDP

.. toctree::
   :maxdepth: 2
   :caption: Interfacing with PSS/PST

   PST

.. toctree::
   :maxdepth: 2
   :caption: Controlling PTP traffic

   PTP

.. toctree::
   :maxdepth: 2
   :caption: Homebrew command line interface

   CLI

.. toctree::
   :maxdepth: 2
   :caption: Monitoring overview

   monitoring

.. toctree::
   :maxdepth: 2
   :caption: Health and Alarms

   health

.. toctree::
   :maxdepth: 2
   :caption: Grafana Dashboard

   dashboard


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
