******************
Interfacing with SPS in AA0.5
******************

In this document, we aim at describing how, in AA0.5, the P4 switch will:

* be connected to SPS;
* respond to legacy protocol, such as ARP, to SPS;
* be connected to the 6 Alveo;
* route LFAA traffic to Alveos;
* be configured to perform LFAA routing.

Overall we are interested in describing the operation of the P4 switch in the scenario depicted below.

.. image:: diagrams/p4_sps_05.png
  :width: 400
  :alt: SPS to CBF Traffic in AA0.5

Hardware
################

In this configuration the P4 switch is using 8 of the 100G ports as follows:

* 6 ports connected to the Alveo server using 100G DAC cables
* 2 ports connected to SPS using 100G QSFP LR

In addition, the P4 switch is using 2 10G ports, one for the Management&Control and one for connection to the PTP switch.

Answering ARP requests from SPS
################

In order to receive data from SPS, Low.CBF might need to answer to ARP requests. These replies are generated inside the I/O switch in Low CBF via a dedicated routing table. In this table, the P4 switch will:

* match a ARP request IP address querry;
* modify the ARP request packet and transform it into a reply packet;
* configure the reply with pre-defined MAC address.

Tango methods
==============

In order to configure the switch for ARP interfacing, 4 methods are publicly available via the Connector Tango.

* AddARPEntry: add one or more entries in the ARP table. This function takes a json string as an argument and configure the Table. In the example below, we configure the ARP table to answer that the computer with the address 192.168.1.1 has the Mac address aa:bb:cc:dd:ee:ff

.. code-block:: json

    {"arp": [{"ip": {"address": "192.168.1.1"}, "mac": {"address": "aa:bb:cc:dd:ee:ff"}}]}

* UpdateARPEntry: this function is similar to the AddARPEntry function and takes the same input format json string.
* ClearARPTable: remove all ARP table entries
* arpRoutingTable: retrieve all ARP table entries

LFAA routing
################

Incoming traffic
==============

In the context of AA0.5, SPS will be constituted of 6 stations. From this stations, it will generate traffic over a
75MHz bandwidth with two 16-bits polarisations oversampled at 32/27. As a results this will generate 17Gbps of raw data.
Computation for the various data rate can be found
`here <https://docs.google.com/spreadsheets/d/1Qza66EnFgSQyeJwhoM_vxqVzMsNe8bHSmg6CI6Q3nSk/edit#gid=978340330>`_.
This raw data is encapsulated in `SPEAD <https://casper.astro.berkeley.edu/astrobaki/images/9/93/SPEADsignedRelease.pdf>`_
packets of 8264 bytes.

Upon reception of SPEAD traffic from SPS, Low CBF P4 switch in AA0.5 will route and potentially multiply SPEAD traffic in function of the configured modus operanti of the telescope. In particular, in AA0.5, we are focusing on the following operation of the CBF:

* PSS beamformer
* PST beamformer
* SDP Correlator

It is noteworthy that in AA0.5 PSS and PST beamformer will be identical from the point of view of the firmware. Also zoom mode is not available in AA0.5.

P4 routing
==============

In order to route the packet to alveos the P4 switch leverage information from the SPEAD header shown below.

.. image:: diagrams/spead_headers.png
  :width: 400
  :alt: SPEAD header

In particular, the P4 switch will route traffic using the <beam_id, frequency_id, sub_array_id> tuple to direct the correct traffic to a given Alveo. This routing consists in potential mode from the switch point of view

* unicast SPEAD routing. In this mode, a <beam_id, frequency_id, sub_array_id> tuple is routed to a single Alveo.
* multiplier SPEAD routing. In this mode, a <beam_id, frequency_id, sub_array_id> tuple is route to multiple Alveos.

Tango methods
==============

In order to configure the route for the correlator and beamformer, 8 methods are publicly available via the Connector Tango.

* AddSpeadUnicastEntry: add one or more entries in the SPEAD Unicast table. This function takes a json string as an argument and configure the Table. In the example below, we configure the SPEAD Unicast table to route every spead packet with frequency_id = 123, beam_id = 12, and sub_array=1 to destination port 12

.. code-block:: json

  '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port": "12/0"}}]}'

* UpdateSpeadUnicastEntry: update one or more entries in the SPEAD Unicast table. This function takes a json string as an argument and configure the Table. In the example below, we update the SPEAD Unicast table to route every spead packet with frequency_id = 123, beam_id = 12, and sub_array=1 to destination port 13 instead of 12.

.. code-block:: json

  '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port": "13/0"}}]}'

* ClearSpeadUnicastTable: remove all SPEAD Unicast table entries
* speadUnicastRoutingTable: retrieve all SPEAD Unicast table entries
* AddSpeadMuliplierEntry: add one or more entries in the Multiplier Multicast table. This function takes a json string as an argument and configure the Table. In the example below, we configure the SPEAD Multiplier table to route every spead packet with frequency_id = 123, beam_id = 12, and sub_array=1 to destination ports 12 and 13

.. code-block:: json

  '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port_bf": "12/0", "port_corr": "13/0"}}]}'

* UpdateSpeadMuliplierEntry: update one or more entries in the Multiplier Multicast table. This function takes a json string as an argument and configure the Table. In the example below, we update the SPEAD Multiplier table to route every spead packet with frequency_id = 123, beam_id = 12, and sub_array=1 to destination ports 14 and 15 instead of 12 and 13

.. code-block:: json

  '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port_bf": "14/0", "port_corr": "15/0"}}]}'

* ClearSpeadMuliplierTable: remove all SPEAD Multiplier table entries
* speadMuliplierRoutingTable: retrieve all SPEAD Multiplier table entries
