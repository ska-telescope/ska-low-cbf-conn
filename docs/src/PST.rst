*********************************
Interfacing with PST/PSS in AA0.5
*********************************

WARNING: This is a work in progress

In this document, we aim at describing how, in AA0.5, the P4 switch will:

* be connected to PSS;
* respond to legacy protocol, such as ARP, to PSS/PST;
* be connected to the 6 Alveo;
* route PSR traffic to PST/PSS;
* be configured to perform LFAA routing.

Overall we are interested in describing the operation of the P4 switch in the scenario depicted below.

.. image:: diagrams/p4_pss_05.png
    :width: 400
    :alt: CBF to PSS/PST Traffic in AA0.5

Hardware
################

In this configuration the P4 switch will use 8 of its 100G ports as follows:

* 6 switch ports connected to 6 Alveo cards using 100G DAC cables
* 1 switch ports connected to PSS/PST using 100G QSFP LR. This 100G port will be split into four 25G links; one for PSS, two for PST, and one unused.

In addition to the 100G ports, two of the P4 switch's 10G management ports will be used - one for the Management&Control and one to carry carry IEEE1588 PTP traffic from the PTP switch.

Answering ARP requests from PST
###############################

PST/PSS servers receiving data from Low.CBF will send ARP requests to find the MAC address corresponding to the IP address of the Low.CBF sending end. Although these could be ignored because traffic will be unidirectional, it is good practice (and may aid debugging) for Low.CBF to provide an ARP response. These replies will be generated inside the I/O switch in Low CBF via a dedicated routing table. In this table, the P4 switch will:

* match a ARP request IP address querry;
* modify the ARP request packet and transform it into a reply packet;
* configure the reply with pre-defined MAC address.

Tango methods
==============

In order to configure the switch for ARP interfacing, 4 methods are publicly available via the Connector Tango.

* AddARPEntry: add one or more entries in the ARP table. This function takes a json string as an argument and configure the Table. In the example below, we configure the ARP table to answer that the computer with the address 192.168.1.1 has the Mac address aa:bb:cc:dd:ee:ff

.. code-block:: json

    {"arp": [{"ip": {"address": "192.168.1.1"}, "mac": {"address": "aa:bb:cc:dd:ee:ff"}}]}

* UpdateARPEntry: this function is similar to the AddARPEntry function and takes the same input format json string.
* ClearARPTable: remove all ARP table entries
* arpRoutingTable: retrieve all ARP table entries

PSR routing
################

Outgoing traffic
================

In the context of AA0.5, PSS and PST beamformer will generate respectively 21.6 and 25.6 Gbps of raw data.
Computation for the various data rate can be found `here <https://docs.google.com/spreadsheets/d/1Qza66EnFgSQyeJwhoM_vxqVzMsNe8bHSmg6CI6Q3nSk/edit#gid=978340330>`_.
This raw data is encapsulated in `PSR packets <https://docs.google.com/document/d/1MMu38QMe7gUuV_bCBYHYfI6VxVaJby7c/edit>`_.

P4 routing
============

In order to route the packet to alveos the P4 switch leverage information from the PSR header shown below.

.. tabularcolumns:: |C|C|C|C|C|C|C|C|C|
.. table:: PSR Header

    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    | Word  | Byte 0 | Byte 1 | Byte 2 | Byte 3 | Byte 4 | Byte 5 | Byte 6 | Byte 7 |
    +=======+========+========+========+========+========+========+========+========+
    |     0 |                         Packet Sequence Number                        |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     1 |                 Timestamp (attoseconds from integer second)           |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     2 | Timestamp (seconds from epoch)    |      Channel Separation (mHz)     |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     3 |                      First Channel Frequency (mHz)                    |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     4 |             Scale #1              |               Scale #2            |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     5 |             Scale #3              |               Scale #4            |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     6 |       First Channel Number        | Channel per pkt | Valid Ch per pkt|
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     7 | No. time sample |   Beam Number   |             Magic Word            |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     8 | Pkt Dst|DataPrec|Pwr Avg |  ts/wt | O/S num|O/S den | Beamformer ver  |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |     9 |                                Scan ID                                |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |    10 |            Offset #1              |               Offset #2           |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+
    |    11 |            Offset #3              |               Offset #4           |
    +-------+--------+--------+--------+--------+--------+--------+--------+--------+

In particular, the P4 switch will route traffic using the beam_id to direct the correct traffic to a given Alveo. This routing consists in potential mode from the switch point of view

* unicast PSR routing. In this mode, a beam_id is routed to a single PSS or PST server.

Tango methods
==============

In order to configure the route for PSS and PST, 4 methods are publicly available via the Connector Tango.

* AddPSREntry: add one or more entries in the PSR table. This function takes a json string as an argument and configure the Table. In the example below, we configure the SPEAD Unicast table to route every spead packet with frequency_id = 123, beam_id = 12, and sub_array=1 to destination port 12 and udp port 9510

.. code-block:: json

    {"psr": [{"src": {"beam": 12 }, "dst": {"port": "12/0", "udp_port": 9510}}]}

* UpdatePSREntry: update one or more entries in the SPEAD Unicast table. This function takes a json string as an argument and configure the Table. In the example below, we update the SPEAD Unicast table to route every spead packet with frequency_id = 123, beam_id = 12, and sub_array=1 to destination port 13 instead of 12 and udp port 9510.

.. code-block:: json

    {"psr": [{"src": {"beam": 12 }, "dst": {"port": "13/0", "udp_port": 9510}}]}

* ClearSpeadUnicastTable: remove all SPEAD Unicast table entries
* speadUnicastRoutingTable: retrieve all SPEAD Unicast table entries
