******************
Control with homebrewed CLI
******************


In addition to the tango control, the Perentie team provides a custom made CLI to control the P4 switch. This CLI is coming automatically with the connector python packages ska-low-cbf-conn and ska-low-cbf-net, with -conn being an extension of -net. In order to start this CLI, one can just download the latest ska-low-cbf-conn package from the CAR or clone the ska-low-cbf-conn repository. After cloning this repository and installing all necessary dependencies with poetry, a user can start this CLI as follows

poetry run connector_cli --bfrt-ip <IP_of_switch> --bfrt-port 50052 --program tna_telemetry


Below we show the interface of the CLI is started from the -conn repository with all methods inherited from the parent package. We detail next these methods.

.. image:: diagrams/homebrew_cli.png
  :width: 400
  :alt: Perentie CLI

Base connector cli (ska-low-cbf-net repo)
############

* ``show_ports "port"``

**Description:** Show List all active ports and some associated metrics such as number of bytes/packets received and sent or number of errors. If a port number and its channel (i.e. 4/1 refers to port number 4, channel 1) front-panel/lane is provided, only that port will be shown.

Return: list of statistics per port


* ``show_simple_table``

Description: Show the “Basic” table.


* ``show_counter_ingress "port"``

Description: Show number of packets per protocol counters associated with ingress ports. If a port number is provided, only that port will be shown.

Otherwise it will show only active ports


* ``show_counter_egress "port"``

Description: Show number of packets per protocol counters associated with egress ports. If a port number is provided, only that port will be shown.

Otherwise it will show only active ports


* ``reset_port_statistics``

Description: Reset port statistics. These statistics relate to the general statistics of any active ports such as the number of bytes/packets received and sent or the number of errors detected in transmission and reception. These statistics are not related to the number of packets/bytes per port and per protocol given by the ingress and egress statistics.


* ``reset_ingress_statistics``

Description: Reset ingress counters


* ``reset_egress_statistics``

Description: Reset egress counters


* ``add_simple_table_entry ingress_port egress_port``

Description: Add “Basic” Table entry. The simple table matches a given ingress port to an egress port.

This function create a entry and take two arguments, first the ingress then egress port.


* ``update_simple_table_entry ingress_port egress_port``

Description: Update Simple Table entry. The simple table matches a given ingress port to an egress port.


* ``load_port yaml_file``

Description: Load ports yaml file and enable switchfront panel ports.


* ``create_multicast_session session_id``

Description: Create a multicast session


* ``configure_multicast_session sid rid yid zid hash1 hash2``

Description: configure a multicast session


* ``add_ingress_to_multicast_session sid port_number``

Description: associate an ingress port to a multicast session


* ``add_port_to_multicast_session sid rid port_number``

Description: Add destination ports in multicast session


* ``get_multicast_sessions_and_port``

Description: get information about multicast sessions


* ``show_arp_counter``

Description: Show Counters from ARP table


* ``show_arp_table``

Description: Show ARP table


* ``arp_table_entry IP_address MAC_Address``

Description: Add ARP Table entry. The simple table matches a given IP address  to an MAC address.

This function creates an entry and takes two arguments, first the IP address then MAC address.


SKA connector cli (ska-low-cbf-conn repo)
###############

* ``show_spead_table``

Description: Show the Spead table.


* ``show_psr_table``

Description: Show the PSR table.


* ``add_spead_table_entry frequency beam sub_array egress_port``

Description: Add SPEAD Table entry. The simple table matches a triple (frequency, beam, sub_array) to an egress port.

This function creates an entry and takes 4 arguments, first the frequency then, beam, number, and then egress port.



* ``update_spead_table_entry frequency beam sub_array egress_port``

Description: Update SPEAD Table entry. The SPEAD table matches a triple (frequency, beam, sub_array) to an egress port.

This function creates an entry and takes 4 arguments, first the frequency then, beam, number, and then egress port.


* ``clear_spead_table``

Description: Remove entries in the spead unicast table


* ``add_spead_entries entries.yaml``

Description:  Add Spead Table entries. Based on a file


* ``show_spead_counter``

Description: show Counters from Spead table.


* ``show_psr_counter``

Description: Show Counters from PSR table.


* ``add_psr_table_entry beam egress_port``

Description: Add PSR Table entry. The PSR table matches a beam number to an egress port.

This function creates an entry and takes two arguments, first the beam number then egress port.


* ``update_psr_table_entry beam egress_port``

Description: Update PSR Table entry. The PSR table matches a beam number to an egress port.

This function updates an entry and takes two arguments, first the beam number then egress port.


* ``add_spead_multicast_table_entry frequency, beam, sub_array bf_port correlator_port``

Description: Add CBF Table entry. The CBF table matches a triple (frequency, beam, sub_array) to 2 ports (one for beamformer, one for correlator).

This function create a entry and take two arguments, first the frequency then, beam, number, and then BF     egress port and finally Correlator egress port.


* ``show_spead_multicast_table``

Description: Show the Correlator and Beamformer table.


* ``update_spead_multicast_table_entry frequency, beam, sub_array bf_port correlator_port``

Description: Update CBF Table entry. The CBF table matches a triple (frequency, beam, sub_array) to 2 ports (one for beamformer, one for correlator).

This function updates a entry and take two arguments, first the frequency then, beam, number, and then BF egress port and finally Correlator egress port.


* ``start_advanced_telemetry interface interval``

Description: Start the daemon for advanced telemetry on a given interface every interval.


* ``get_spead_throughput``

Description: Return the throughput for each (frequency, beam, sub_array) tuple computed by the advanced telemetry.


* ``get_spead_bytes``

Description: Return the total number of bytes for each (frequency, beam, sub_array) tuple computed by the advanced telemetry