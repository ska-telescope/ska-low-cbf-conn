# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import binascii
import ipaddress
import logging
import re

from ska_low_cbf_net.connector_protocol import Protocol


class SDP_MAC(Protocol):
    def __init__(self, target, gc, bfrt_info):
        """
        Initialisation of the general class related to the change_mac_dst table
        This is initialised from the connector in general
        :param target: pipe configuration of the P4 switch return by the bfrt client
        :param gc: the bfrt client from the Intel library
        :param bfrt_info: all tables configured on the switch
        """
        # Set up base class
        super(SDP_MAC, self).__init__(target, gc)
        self.table = bfrt_info.table_get("change_mac_dst_table")

    def _clear(self):
        """Remove all entries"""
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            self.table.entry_del(
                self.target,
                [
                    self.table.make_key(
                        [
                            self.gc.KeyTuple(
                                "sdp_ip",
                                key.to_dict()["sdp_ip"]["value"],
                            ),
                        ]
                    )
                ],
            )

    def clear(self):
        self._clear()

    def add_entry(self, match, action):
        """Add one entry.

        Keyword arguments:
        :param action: Mac destination address to change in the packet
        :param match: IP Address
        """
        ip_address = int(ipaddress.IPv4Address(match))
        mac_address = int.from_bytes(
            binascii.unhexlify(action.replace(":", "")),
            byteorder="big",
            signed=False,
        )
        self.table.entry_add(
            self.target,
            [
                self.table.make_key(
                    [
                        self.gc.KeyTuple("sdp_ip", ip_address),
                    ]
                )
            ],
            [
                self.table.make_data(
                    [self.gc.DataTuple("mac_add", mac_address)],
                    "change_mac_dst",
                )
            ],
        )

    def remove_entry(self, match):
        """Remove one entry
        :param match: IP Address
        """
        ip_address = int(ipaddress.IPv4Address(match))
        self.table.entry_del(
            self.target,
            [
                self.table.make_key(
                    [
                        self.gc.KeyTuple("sdp_ip", ip_address),
                    ]
                )
            ],
        )

    def get_match(self, action):
        """Get IP Address(es) for a Mac address.
        :param action: Mac destination address to change in the packet
        Returns:
            (success flag, IP Address(es) or error message)
        """
        entries = []
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if data.to_dict()["mac_add"] is action:
                freq_beam_sub_array = "{}".format(
                    key.to_dict()["sdp_ip"]["value"],
                )
                entries.append(freq_beam_sub_array)
        if len(entries) == 0:
            return (False, "MAC Address not found")

        return (True, entries)

    def get_action(self, match):
        """Get Mac address for an IP Address.

        :param match: IP Address
        Returns:
            (success flag, Mac address or error message)
        """
        ip_address = int(ipaddress.IPv4Address(match))
        results = self.table.entry_get(
            self.target,
            [
                self.table.make_key(
                    [
                        self.table.make_key(
                            [self.gc.KeyTuple("sdp_ip", ip_address)]
                        ),
                    ]
                )
            ],
        )

        return results

    def get_entries(self):
        """Get all forwarding entries.

        Returns:
            list of (IP address, Mac address)
        """
        entries = {}
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            entries[
                ipaddress.IPv4Address(key.to_dict()["sdp_ip"]["value"])
            ] = ":".join(re.findall("..", "%012x" % data.to_dict()["mac_add"]))
        return entries

    def reset_counters(self):
        """Reset SDP MAC counters"""

        # Reset direct counter
        self.table.operations_execute(self.target, "SyncCounters")
        resp = self.table.entry_get(self.target, flags={"from_hw": False})

        keys = []
        values = []

        for v, k in resp:
            keys.append(k)

            v = v.to_dict()
            k = k.to_dict()

            values.append(
                self.table.make_data(
                    [
                        self.gc.DataTuple("$COUNTER_SPEC_BYTES", 0),
                        self.gc.DataTuple("$COUNTER_SPEC_PKTS", 0),
                    ],
                    v["action_name"],
                )
            )

        self.table.entry_mod(self.target, keys, values)

    def update_entry(self, match, action):
        """
        Update an entry in the table
        Keyword arguments:
        :param action: Mac destination address to change in the packet
        :param match: IP Address
        """
        installed_action = False
        entries_from_switch = self.table.entry_get(self.target)
        ip_address = int(ipaddress.IPv4Address(match))
        mac_address = int.from_bytes(
            binascii.unhexlify(action.replace(":", "")),
            byteorder="big",
            signed=False,
        )
        for data, key in entries_from_switch:
            if int(key.to_dict()["sdp_ip"]["value"]) == ip_address:
                installed_action = True
        if installed_action:
            self.table.entry_mod(
                self.target,
                [
                    self.table.make_key(
                        [
                            self.gc.KeyTuple("sdp_ip", ip_address),
                        ]
                    )
                ],
                [
                    self.table.make_data(
                        [self.gc.DataTuple("mac_add", mac_address)],
                        "change_mac_dst",
                    )
                ],
            )
        else:
            self.add_entry(match, action)
