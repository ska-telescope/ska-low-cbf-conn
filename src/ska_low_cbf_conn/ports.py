#  Copyright 2021 CSIRO
#
# inspired from the switch_ml project

from bfrt_grpc.client import BfruntimeRpcException
from ska_low_cbf_net.ports import Ports


class PortTof2(Ports):
    def __init__(self, target, gc, bfrt_info):

        super(PortTof2, self).__init__(target, gc, bfrt_info)

    def get_stats(self, front_panel_port=None, lane=None):
        """Get active ports statistics.
        If a port/lane is provided, it will return only stats of that port.

        Keyword arguments:
            front_panel_port -- front panel port number
            lane -- lane within the front panel port
        Returns:
            (success flag, stats or error message)
        """

        if front_panel_port:
            if not lane:
                lane = 0

            success, dev_port = self.get_dev_port(front_panel_port, lane)
            if not success:
                return (False, dev_port)

            dev_ports = [dev_port]

            if dev_port not in self.active_ports:
                return (
                    False,
                    "Port {}/{} not active".format(front_panel_port, lane),
                )
        else:
            if self.active_ports:
                dev_ports = self.active_ports
            else:
                return (False, "No active ports")

        # Get stats
        stats_result = self.port_stats_table.entry_get(
            self.target,
            [
                self.port_stats_table.make_key(
                    [self.gc.KeyTuple("$DEV_PORT", i)]
                )
                for i in dev_ports
            ],
            {"from_hw": False},
        )

        # Construct stats dict indexed by dev_port
        stats = {}
        for v, k in stats_result:
            v = v.to_dict()
            k = k.to_dict()
            dev_port = k["$DEV_PORT"]["value"]
            stats[dev_port] = v

        # Get port info
        ports_info = self.port_table.entry_get(self.target, None)
        # ports_info = self.port_table.entry_get(
        #    self.target,
        #    [
        #        self.port_table.make_key([self.gc.KeyTuple("$DEV_PORT", i)])
        #        for i in dev_ports
        #    ],
        # {"from_hw": False},
        # )

        # Combine ports info and statistics
        values = []
        for v, k in ports_info:
            v = v.to_dict()
            k = k.to_dict()
            if k["$DEV_PORT"]["value"] in dev_ports:
                # Insert dev_port into result dict
                dev_port = k["$DEV_PORT"]["value"]
                v["$DEV_PORT"] = dev_port

                # Remove prefixes from FEC and SPEED
                v["$FEC"] = v["$FEC"][len("BF_FEC_TYP_") :]
                v["$SPEED"] = v["$SPEED"][len("BF_SPEED_") :]

                # Add port stats
                v["bytes_received"] = stats[dev_port][
                    "$OctetsReceivedinGoodFrames"
                ]
                v["packets_received"] = stats[dev_port]["$FramesReceivedOK"]
                v["errors_received"] = stats[dev_port]["$FrameswithanyError"]
                v["FCS_errors_received"] = stats[dev_port][
                    "$FramesReceivedwithFCSError"
                ]
                v["bytes_sent"] = stats[dev_port][
                    "$OctetsTransmittedwithouterror"
                ]
                v["packets_sent"] = stats[dev_port]["$FramesTransmittedOK"]
                v["errors_sent"] = stats[dev_port][
                    "$FramesTransmittedwithError"
                ]
                v["TX_PPS"] = stats[dev_port]["$TX_PPS"]
                v["RX_PPS"] = stats[dev_port]["$RX_PPS"]
                v["TX_RATE"] = stats[dev_port]["$TX_RATE"]
                v["RX_RATE"] = stats[dev_port]["$RX_RATE"]

                # Add to combined list
                values.append(v)

        # Sort by front panel port/lane
        values.sort(key=lambda x: (x["$CONN_ID"], x["$CHNL_ID"]))

        return (True, values)

    def reset_stats(self):
        """Reset statistics of all ports"""

        self.port_stats_table.entry_mod(
            self.target,
            [
                self.port_stats_table.make_key(
                    [self.gc.KeyTuple("$DEV_PORT", i)]
                )
                for i in self.active_ports
            ],
            [
                self.port_stats_table.make_data(
                    [
                        self.gc.DataTuple("$FramesReceivedOK", 0),
                        self.gc.DataTuple("$FramesReceivedAll", 0),
                        self.gc.DataTuple("$OctetsReceivedinGoodFrames", 0),
                        self.gc.DataTuple("$FrameswithanyError", 0),
                        self.gc.DataTuple("$FramesReceivedwithFCSError", 0),
                        self.gc.DataTuple("$FramesTransmittedOK", 0),
                        self.gc.DataTuple("$FramesTransmittedAll", 0),
                        self.gc.DataTuple("$OctetsTransmittedwithouterror", 0),
                        self.gc.DataTuple("$FramesTransmittedwithError", 0),
                    ]
                )
            ]
            * len(self.active_ports),
        )

    def add_port_mapping(self, board_num, port_num, lane_num):
        """
        Add port mapping manually.
        :param board_num: board number
        :param port_num: port number
        :param lane_num: lane number
        """
        success, ing_fp_port, ing_fp_lane = self.get_fp_port(board_num)
        if success:
            self.log.info(
                f"Port already in the CPU mapping at {ing_fp_port}/{ing_fp_lane}"
            )
        else:
            self.dev_port_to_fp_port[board_num] = (port_num, lane_num)
