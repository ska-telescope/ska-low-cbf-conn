class ClientInterface:
    def __init__(self, address, client_id, device_id):
        self.address = address
        self.client_id = client_id
        self.device_id = device_id

    def bind_pipeline_config(self, program):
        self.program = program

    def bfrt_info_get(self, program):
        return program


class Target:
    def __init__(self, dev, pipe_id):
        self.dev = dev
        self.pipe_id = pipe_id


class BfruntimeRpcException(Exception):
    def __init__(self):
        self.exception = True


class BfruntimeForwardingRpcException(Exception):
    def __init__(self):
        self.exception = True
