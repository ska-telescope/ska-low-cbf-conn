# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=import-error
"""The Python implementation of the GRPC bfshell client."""

from __future__ import print_function

import json

import bfshell_pb2
import bfshell_pb2_grpc
import grpc

BFSHELL_PORT = 50051
BFSHELL_ADDRESS = "202.9.15.139"


def run():
    """Start the simple bfshell client."""
    print("Will try to greet world ...")
    with grpc.insecure_channel(f"{BFSHELL_ADDRESS}:{BFSHELL_PORT}") as channel:
        stub = bfshell_pb2_grpc.BFshellStub(channel)
        response = stub.get_qsfp_serials(bfshell_pb2.Empty())
        print("==== List of QFSP serial ====")
        print("BFShellReader client received: " + response.message)
        print("==== Temperature of QFSPs ====")
        for port_number in json.loads(response.message).keys():
            response_temp = stub.get_qsfp_temp_power(
                bfshell_pb2.ShellRequest(name=f"{port_number}")
            )
            print("BFShellReader client received: " + response_temp.message)
        response = stub.get_serial(bfshell_pb2.Empty())
        print("==== Overall Serial number ====")
        print("BFShellReader client received: " + response.message)
        response = stub.get_fan_speed(bfshell_pb2.Empty())
        print("==== Overall Fan Speed ====")
        print("BFShellReader client received: " + response.message)
        response = stub.get_general_temp(bfshell_pb2.Empty())
        print("==== Overall Temperature ====")
        print("BFShellReader client received: " + response.message)


if __name__ == "__main__":
    run()
