# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=import-error,too-many-instance-attributes,unused-variable,W0613
"""The Python implementation of the GRPC bfshell server."""

import json
import logging
import re
from concurrent import futures
from copy import deepcopy
from threading import Thread
from time import sleep

import bfshell_pb2
import bfshell_pb2_grpc
import grpc
import parse
import pexpect


class BFShellReader(bfshell_pb2_grpc.BFshellServicer):
    """BF Shell Reader gRPC server."""

    def __init__(self):
        """Initialise the class."""
        self.child = pexpect.spawn("/opt/intel/bf-sde/install/bin/bfshell")
        self.child.expect("bfshell")
        self.child.sendline("ucli")
        self.child.expect("bf-sde")
        self._answer_fan = {}
        self._answer_general_temp = {}
        self._answer_qsfp_serial = {}
        self._answer_qsfp_temp_power = {}
        self._answer_qsfp_power = {}
        self._answer_serial = {"serial": "unknown"}
        self._monitoring_thread = Thread(target=self.monitor_loop)
        self._monitoring_thread.start()

    def monitor_loop(self):
        """Monitor the switch in a loop."""
        every_five = 0
        while True:
            self._get_qsfp_serials_shell()
            for port_number, serial in self._answer_qsfp_serial.items():
                self._get_qsfp_temp_power_shell(port_number)
            if every_five == 0:
                self._get_fan_speed_shell()
                self._get_general_temp_shell()
                self._get_serial_shell()
            every_five = (every_five + 1) % 5

            sleep(5)

    def _get_qsfp_serials_shell(self):
        """Retrieve QSFP serial numbers from the shell."""
        self.child.sendline("bf_pltfm qsfp info")
        self.child.expect("bf-sde")
        response = self.child.before.decode()
        answer = {}
        for index, line in enumerate(response.split("\n")):
            parser = parse.parse(
                "{port_num}: {Vendor} {PN} {rev} {Serial} {Date} "
                + "{Rate} MBps {OUI} {PowerClass} {MediaClass}",
                re.sub(" +", " ", line),
            )
            if parser is not None:
                answer[re.sub(" +", "", parser["port_num"])] = parser["Serial"]
        self._answer_qsfp_serial = deepcopy(answer)

    def get_qsfp_serials(self, request, context):
        """
        Send QSFP serial numbers.

        :param request: gRPC mandatory request, not use here
        :param context: gRPC mandatory context, not use here
        """
        return bfshell_pb2.ShellReply(
            message=json.dumps(self._answer_qsfp_serial)
        )

    def _get_serial_shell(self):
        """Retrieve switch serial number from the shell."""
        self.child.sendline("bf_pltfm chss_mgmt eeprom_data")
        self.child.expect("bf-sde")
        response = self.child.before.decode()
        answer = {"serial": "unknown"}
        for index, line in enumerate(response.split("\n")):
            parser = parse.parse(
                "Product Serial Number: {Serial}",
                re.sub(" +", " ", line.strip()),
            )
            if parser is not None:
                answer["serial"] = parser["Serial"]
        self._answer_serial = deepcopy(answer)

    def get_serial(self, request, context):
        """
        Send switch serial number.

        :param request: gRPC mandatory request, not use here
        :param context: gRPC mandatory context, not use here
        """
        return bfshell_pb2.ShellReply(message=json.dumps(self._answer_serial))

    def _get_qsfp_temp_power_shell(self, port_num):
        """Retrieve temperature and power per QSFP from the shell."""
        self.child.sendline(f"bf_pltfm qsfp dump-info {port_num}")
        self.child.expect("bf-sde")
        response = self.child.before.decode()
        answer = {}
        answer[port_num] = {}
        for index, line in enumerate(response.split("\n")):
            parser_tmp = parse.parse(
                " Temperature: {temp} C{other}", re.sub(" +", " ", line)
            )
            if parser_tmp is not None:
                answer[port_num]["temperature"] = parser_tmp["temp"]
            parser_ch_1 = parse.parse(
                " Channel 1: {rx_power} {tx_power} {other}",
                re.sub(" +", " ", line),
            )
            if parser_ch_1 is not None:
                answer[port_num]["channel_1_rx"] = parser_ch_1["rx_power"]
                answer[port_num]["channel_1_tx"] = parser_ch_1["tx_power"]
            parser_ch_2 = parse.parse(
                " Channel 2: {rx_power} {tx_power} {other}",
                re.sub(" +", " ", line),
            )
            if parser_ch_2 is not None:
                answer[port_num]["channel_2_rx"] = parser_ch_2["rx_power"]
                answer[port_num]["channel_2_tx"] = parser_ch_2["tx_power"]
            parser_ch_3 = parse.parse(
                " Channel 3: {rx_power} {tx_power} {other}",
                re.sub(" +", " ", line),
            )
            if parser_ch_3 is not None:
                answer[port_num]["channel_3_rx"] = parser_ch_3["rx_power"]
                answer[port_num]["channel_3_tx"] = parser_ch_3["tx_power"]
            parser_ch_4 = parse.parse(
                " Channel 4: {rx_power} {tx_power} {other}",
                re.sub(" +", " ", line),
            )
            if parser_ch_4 is not None:
                answer[port_num]["channel_4_rx"] = parser_ch_4["rx_power"]
                answer[port_num]["channel_4_tx"] = parser_ch_4["tx_power"]
        self._answer_qsfp_temp_power[port_num] = deepcopy(answer[port_num])

    def get_qsfp_temp_power(self, request, context):
        """
        Send temperature and power per QSFP.

        :param request: gRPC mandatory request, contains the port number
        :param context: gRPC mandatory context, not use here
        """
        response = {request.name: self._answer_qsfp_temp_power[request.name]}
        return bfshell_pb2.ShellReply(message=json.dumps(response))

    def _get_fan_speed_shell(self):
        """Retrieve fan speed from the shell."""
        self.child.sendline("bf_pltfm chss_mgmt fan_show")
        self.child.expect("bf-sde")
        response = self.child.before.decode()
        answer = {}
        for index, line in enumerate(response.split("\n")):
            parser_tmp_1 = parse.parse(
                "1 {front_rpm} {rear_rpm} {speed_percent}",
                re.sub(" +", " ", line.strip()),
            )
            if parser_tmp_1 is not None:
                answer["fan_1"] = {}
                answer["fan_1"]["front_rpm"] = parser_tmp_1["front_rpm"]
                answer["fan_1"]["rear_rpm"] = parser_tmp_1["rear_rpm"]
                answer["fan_1"]["speed_percent"] = parser_tmp_1[
                    "speed_percent"
                ]
            parser_tmp_2 = parse.parse(
                "2 {front_rpm} {rear_rpm} {speed_percent}",
                re.sub(" +", " ", line.strip()),
            )
            if parser_tmp_2 is not None:
                answer["fan_2"] = {}
                answer["fan_2"]["front_rpm"] = parser_tmp_2["front_rpm"]
                answer["fan_2"]["rear_rpm"] = parser_tmp_2["rear_rpm"]
                answer["fan_2"]["speed_percent"] = parser_tmp_2[
                    "speed_percent"
                ]
            parser_tmp_3 = parse.parse(
                "3 {front_rpm} {rear_rpm} {speed_percent}",
                re.sub(" +", " ", line.strip()),
            )
            if parser_tmp_3 is not None:
                answer["fan_3"] = {}
                answer["fan_3"]["front_rpm"] = parser_tmp_3["front_rpm"]
                answer["fan_3"]["rear_rpm"] = parser_tmp_3["rear_rpm"]
                answer["fan_3"]["speed_percent"] = parser_tmp_3[
                    "speed_percent"
                ]
            parser_tmp_4 = parse.parse(
                "4 {front_rpm} {rear_rpm} {speed_percent}",
                re.sub(" +", " ", line.strip()),
            )
            if parser_tmp_4 is not None:
                answer["fan_4"] = {}
                answer["fan_4"]["front_rpm"] = parser_tmp_4["front_rpm"]
                answer["fan_4"]["rear_rpm"] = parser_tmp_4["rear_rpm"]
                answer["fan_4"]["speed_percent"] = parser_tmp_4[
                    "speed_percent"
                ]
            parser_tmp_5 = parse.parse(
                "5 {front_rpm} {rear_rpm} {speed_percent}",
                re.sub(" +", " ", line.strip()),
            )
            if parser_tmp_5 is not None:
                answer["fan_5"] = {}
                answer["fan_5"]["front_rpm"] = parser_tmp_5["front_rpm"]
                answer["fan_5"]["rear_rpm"] = parser_tmp_5["rear_rpm"]
                answer["fan_5"]["speed_percent"] = parser_tmp_5[
                    "speed_percent"
                ]
            parser_tmp_6 = parse.parse(
                "6 {front_rpm} {rear_rpm} {speed_percent}",
                re.sub(" +", " ", line.strip()),
            )
            if parser_tmp_6 is not None:
                answer["fan_6"] = {}
                answer["fan_6"]["front_rpm"] = parser_tmp_6["front_rpm"]
                answer["fan_6"]["rear_rpm"] = parser_tmp_6["rear_rpm"]
                answer["fan_6"]["speed_percent"] = parser_tmp_6[
                    "speed_percent"
                ]

        self._answer_fan = deepcopy(answer)

    def get_fan_speed(self, request, context):
        """
        Send fan speed.

        :param request: gRPC mandatory request, not use here
        :param context: gRPC mandatory context, not use here
        """
        return bfshell_pb2.ShellReply(message=json.dumps(self._answer_fan))

    def _get_general_temp_shell(self):
        """Retrieve general temperatures from the shell."""
        self.child.sendline("bf_pltfm chss_mgmt tmp_show")
        self.child.expect("bf-sde")
        response = self.child.before.decode()
        answer = {}
        for index, line in enumerate(response.split("\n")):
            parser_tmp_1 = parse.parse(
                "tmp1 {temperature} C", re.sub(" +", " ", line.strip())
            )
            if parser_tmp_1 is not None:
                answer["cpu_sensor_1"] = parser_tmp_1["temperature"]
            parser_tmp_2 = parse.parse(
                "tmp2 {temperature} C", re.sub(" +", " ", line.strip())
            )
            if parser_tmp_2 is not None:
                answer["cpu_sensor_2"] = parser_tmp_2["temperature"]
            parser_tmp_3 = parse.parse(
                "tmp3 {temperature} C", re.sub(" +", " ", line.strip())
            )
            if parser_tmp_3 is not None:
                answer["cpu_sensor_3"] = parser_tmp_3["temperature"]
            parser_tmp_4 = parse.parse(
                "tmp4 {temperature} C", re.sub(" +", " ", line.strip())
            )
            if parser_tmp_4 is not None:
                answer["cpu_sensor_4"] = parser_tmp_4["temperature"]
            parser_tmp_5 = parse.parse(
                "tmp5 {temperature} C", re.sub(" +", " ", line.strip())
            )
            if parser_tmp_5 is not None:
                answer["cpu_sensor_5"] = parser_tmp_5["temperature"]
            parser_tmp_10 = parse.parse(
                "tmp10 {temperature} C", re.sub(" +", " ", line.strip())
            )
            if parser_tmp_10 is not None:
                answer["tofino_board_sensor"] = parser_tmp_10["temperature"]
        self._answer_general_temp = deepcopy(answer)

    def get_general_temp(self, request, context):
        """
        Send general temperatures.

        :param request: gRPC mandatory request, not use here
        :param context: gRPC mandatory context, not use here
        """
        return bfshell_pb2.ShellReply(
            message=json.dumps(self._answer_general_temp)
        )


def serve():
    """Start the server."""
    port = "50051"
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    bfshell_pb2_grpc.add_BFshellServicer_to_server(BFShellReader(), server)
    server.add_insecure_port("[::]:" + port)
    server.start()
    print("Server started, listening on " + port)
    server.wait_for_termination()


if __name__ == "__main__":
    logging.basicConfig()
    serve()
