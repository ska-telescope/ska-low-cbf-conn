# -*- coding: utf-8 -*-
#
# (c) 2024 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Abstract base class for processor health affecting attributes
"""

from abc import ABC
from time import time

from ska_control_model import HealthState
from tango import AttrQuality


class HealthAttr(ABC):
    "Base class for health attributes"

    def __init__(self, name: str, qual_updater=None):
        """
        :param name: health attribute name
        :param qual_updater: a function to call when updating quality factor;
                             probably depends on factors beyond the scope of
                             this class
        """
        self.name = name
        self.quality = AttrQuality.ATTR_VALID
        self.value = None
        self.mod_time = 0.0
        self.health = HealthState.OK
        self.quality_updater = qual_updater

    def update_health(self) -> None:
        """Override in a derived class

        Recalculate the health contribution of this attribute based on its
        current value and quality factor"""

    def update_quality(self) -> bool:
        """Refresh AttrQuality for this attribute based on some external
        factor encapsulated in quality_updater member

        :return: True if quality factor has changed, False otherwise
        """
        if self.quality_updater:
            qual = self.quality_updater()
            if self.quality != qual:
                self.quality = qual
                self.update_health()
                return True
        return False

    def set_value(self, value) -> bool:
        """Set this attributes value.

        :param value: current value
        :return: True if the value has changed, False otherwise
        """
        if self.value != value:
            self.value = value
            self.mod_time = time()
            self.update_health()
            return True
        return False

    @property
    def value_date_quality(self) -> tuple:
        """Return this attribute's (value, modification time, quality) tuple
        Used by Tango attributes that care about attribute's quality.
        """
        return self.value, self.mod_time, self.quality
