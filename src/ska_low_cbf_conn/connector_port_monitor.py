# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=import-error

"""Provides monitor object and various monitoring functions."""
import copy
import glob
import json
import logging
import os
import sys
from copy import deepcopy
from threading import Thread
from time import sleep

import grpc
import numpy as np

import ska_low_cbf_conn.bfshell.bfshell_pb2
import ska_low_cbf_conn.bfshell.bfshell_pb2_grpc

NUMBER_OF_PHYSICAL_PORTS = 32

if "SDE_INSTALL" in os.environ and os.path.exists(os.environ["SDE_INSTALL"]):
    bfrt_location = "{}/lib/python*/site-packages/".format(
        os.environ["SDE_INSTALL"]
    )
    sys.path.append(glob.glob(bfrt_location)[0])
else:
    mockup_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "mockup"
    )
    sys.path.append(mockup_path)

import pltfm_mgr_rpc.pltfm_mgr_rpc as pltfm_mgr
from thrift.protocol import TBinaryProtocol, TMultiplexedProtocol
from thrift.transport import TSocket, TTransport


class SwitchMonitor:
    """SwitchMonitor class."""

    def __init__(
        self,
        tango_device,
        connector,
    ):
        """Init function."""
        self.logger = logging.getLogger(__name__)
        self.tango_device = tango_device
        self.connector = connector
        self._serial_number = ""
        self._health_status = {}
        self._average_temperature = 0.0
        self._tofino_temperature = 0.0
        self._qsfp_tx_power = {}
        self._qsfp_rx_power = {}
        self._qsfp_temperature = {}
        self._qsfp_serials = {}
        self._rx_throughput = {}
        self._tx_throughput = {}
        self._rx_pps = {}
        self._tx_pps = {}
        self._up = {}
        self._sps_rates = {}
        for port in range(1, 33):
            self._qsfp_tx_power[f"{port}"] = "-40.0, -40.0, -40.0, -40.0"
            self._qsfp_rx_power[f"{port}"] = "-40.0, -40.0, -40.0, -40.0"
            self._qsfp_temperature[f"{port}"] = 0
            self._qsfp_serials[f"{port}"] = "NotRetrieved"

    def start_shell_monitoring(self, bfrt_shell_address):
        """Start the shell monitoring"""
        self._shell_registration = Thread(
            target=self.update_information_from_grpc,
            args=(bfrt_shell_address,),
        )
        self._shell_registration.start()

    def start_health_monitoring(self):
        """Start the health monitoring"""
        self._health_status_thread = Thread(
            target=self.update_health_status_and_state,
        )
        self._health_status_thread.start()

    def start_advanced_telemetry_monitoring(self):
        """Start the advanced telemetry monitoring"""
        self._advanced_sps_thread = Thread(
            target=self.advanced_sps_telemetry,
        )
        self._advanced_sps_thread.start()

    def update_health_status_and_state(self):
        """Get new attribute health state and status then push event if required"""

        while True:
            if self.connector is not None:
                health_status = self.connector.get_health_status()
                if self._health_status != health_status:
                    self.tango_device.push_change_event(
                        "health_status", json.dumps(health_status)
                    )
                    self.tango_device.push_archive_event(
                        "health_status", json.dumps(health_status)
                    )
                    # self.logger.info("New Health Status published")
                    self.extract_throughput_from_health(health_status)
                    self._health_status = deepcopy(health_status)
                    self.tango_device.update_health_status_and_state(
                        self._health_status
                    )
                # if "ports" in health_status:
                #     expected_health_status = 0
                #     for key, value in health_status["ports"].items():
                #         if int(value["Enable"]) == 0 or int(value["Up"]) == 0:
                #             expected_health_status = 1
                #     self.tango_device.write_healthState(expected_health_status)
            sleep(1)

    def extract_throughput_from_health(self, health_status):
        """Extract throughput and PPS from health status"""
        rx_pps = {}
        tx_pps = {}
        rx_throughput = {}
        tx_throughput = {}
        up = {}
        for port in range(1, NUMBER_OF_PHYSICAL_PORTS + 1):
            rx_throughput[f"{port}/0"] = 0
            tx_throughput[f"{port}/0"] = 0
            rx_pps[f"{port}/0"] = 0
            tx_pps[f"{port}/0"] = 0
            up[f"{port}/0"] = 0
        if "ports" in health_status:
            for key, value in health_status["ports"].items():
                rx_pps[key] = value["RX_PPS"]
                tx_pps[key] = value["TX_PPS"]
                rx_throughput[key] = value["RX_RATE"]
                tx_throughput[key] = value["TX_RATE"]
                up[key] = int(value["Up"])
                port_num = key.split("/")[0]
                if 0 < int(port_num) < (NUMBER_OF_PHYSICAL_PORTS + 1):
                    self.tango_device.push_change_event(
                        f"diagnostics_port_{port_num}_rxpps", rx_pps[key]
                    )
                    self.tango_device.push_archive_event(
                        f"diagnostics_port_{port_num}_rxpps", rx_pps[key]
                    )
                    self.tango_device.push_change_event(
                        f"diagnostics_port_{port_num}_txpps", tx_pps[key]
                    )
                    self.tango_device.push_archive_event(
                        f"diagnostics_port_{port_num}_txpps", tx_pps[key]
                    )
                    self.tango_device.push_change_event(
                        f"process_port_{port_num}_rxthroughput",
                        rx_throughput[key],
                    )
                    self.tango_device.push_archive_event(
                        f"process_port_{port_num}_rxthroughput",
                        rx_throughput[key],
                    )
                    self.tango_device.push_change_event(
                        f"process_port_{port_num}_txthroughput",
                        tx_throughput[key],
                    )
                    self.tango_device.push_archive_event(
                        f"process_port_{port_num}_txthroughput",
                        tx_throughput[key],
                    )
                    self.tango_device.push_change_event(
                        f"diagnostics_port_{port_num}_up", up[key]
                    )
                    self.tango_device.push_archive_event(
                        f"diagnostics_port_{port_num}_up", up[key]
                    )

            if rx_pps != {}:
                self.tango_device.push_change_event(
                    "port_rx_throughput", json.dumps(rx_throughput)
                )
                self.tango_device.push_archive_event(
                    "port_rx_throughput", json.dumps(rx_throughput)
                )
                self._rx_throughput = deepcopy(rx_throughput)
                self.tango_device.push_change_event(
                    "port_tx_throughput", json.dumps(tx_throughput)
                )
                self.tango_device.push_archive_event(
                    "port_tx_throughput", json.dumps(tx_throughput)
                )
                self._tx_throughput = deepcopy(tx_throughput)
                self.tango_device.push_change_event(
                    "port_rx_pps", json.dumps(rx_pps)
                )
                self.tango_device.push_archive_event(
                    "port_rx_pps", json.dumps(rx_pps)
                )
                self._rx_pps = deepcopy(rx_pps)
                self.tango_device.push_change_event(
                    "port_tx_pps", json.dumps(tx_pps)
                )
                self.tango_device.push_archive_event(
                    "port_tx_pps", json.dumps(tx_pps)
                )
                self._tx_pps = deepcopy(tx_pps)
                self._up = deepcopy(up)
                # self.logger.info("New Throughput published")
                self.tango_device.update_from_monitor(
                    self._rx_throughput,
                    self._tx_throughput,
                    self._rx_pps,
                    self._tx_pps,
                    self._up,
                )

    def update_information_from_grpc(self, grpc_address: str):
        """Get information from the gRPC in 9.13.2."""

        transport = TTransport.TBufferedTransport(
            TSocket.TSocket(grpc_address.split(":")[0], 9090)
        )
        bprotocol = TBinaryProtocol.TBinaryProtocol(transport)
        client = pltfm_mgr.Client(
            TMultiplexedProtocol.TMultiplexedProtocol(
                bprotocol, "pltfm_mgr_rpc"
            )
        )
        while True:
            # general monitoring point
            transport.open()
            try:
                temperature_board = client.pltfm_mgr_sys_tmp_get()
                self._tofino_temperature = int(
                    np.round(temperature_board.tmp6)
                )
                self._average_temperature = int(
                    np.round(
                        (
                            temperature_board.tmp1
                            + temperature_board.tmp2
                            + temperature_board.tmp3
                            + temperature_board.tmp4
                            + temperature_board.tmp5
                        )
                        / 5
                    )
                )

            finally:
                transport.close()

            transport.open()
            try:
                board_info = client.pltfm_mgr_sys_eeprom_get()
                self._serial_number = board_info.prod_ser_num
            finally:
                transport.close()

            transport.open()
            qsfp_rx_power = {}
            qsfp_tx_power = {}
            qsfp_temp = {}
            for port in range(1, 33):
                if client.pltfm_mgr_qsfp_detect_transceiver(port):
                    try:
                        qsfp_rx_power[f"{port}"] = ", ".join(
                            str(round(x, 1))
                            for x in 10
                            * np.log10(
                                client.pltfm_mgr_qsfp_chan_rx_pwr_get(port)
                            )
                        )

                    except:
                        qsfp_rx_power[f"{port}"] = "-40.0, -40.0, -40.0, -40.0"
                    try:
                        qsfp_tx_power[f"{port}"] = ", ".join(
                            str(round(x, 1))
                            for x in 10
                            * np.log10(
                                client.pltfm_mgr_qsfp_chan_tx_pwr_get(port)
                            )
                        )
                    except:
                        qsfp_tx_power[f"{port}"] = "-40.0, -40.0, -40.0, -40.0"

                    try:
                        qsfp_temp[f"{port}"] = int(
                            np.round(
                                client.pltfm_mgr_qsfp_temperature_get(port)
                            )
                        )
                    except:
                        qsfp_temp[f"{port}"] = 0.0
                else:
                    qsfp_rx_power[f"{port}"] = "-40.0, -40.0, -40.0, -40.0"
                    qsfp_tx_power[f"{port}"] = "-40.0, -40.0, -40.0, -40.0"
                    qsfp_temp[f"{port}"] = 0.0
            transport.close()
            transport.open()
            qsfp_serial = {}
            for port in range(1, 33):
                if client.pltfm_mgr_qsfp_detect_transceiver(port):
                    try:
                        info = client.pltfm_mgr_qsfp_info_get(port)
                        # serial number is encoded in the info starting at byte 392 and
                        # finished when 0x202020 starts.
                        second_index = info[392:-1].index("202020")
                        qsfp_serial[f"{port}"] = bytes.fromhex(
                            info[392 : (392 + second_index)]
                        ).decode()
                    except:
                        qsfp_serial[f"{port}"] = "No-Info"
                else:
                    qsfp_serial[f"{port}"] = "No-Info"
            transport.close()
            self._qsfp_serials = copy.deepcopy(qsfp_serial)
            self._qsfp_temperature = copy.deepcopy(qsfp_temp)
            self._qsfp_rx_power = copy.deepcopy(qsfp_rx_power)
            self._qsfp_tx_power = copy.deepcopy(qsfp_tx_power)
            for port_number in range(1, 33):
                self.tango_device.push_change_event(
                    f"diagnostics_port_{port_number}_serial",
                    self._qsfp_serials[f"{port_number}"],
                )
                self.tango_device.push_archive_event(
                    f"diagnostics_port_{port_number}_serial",
                    self._qsfp_serials[f"{port_number}"],
                )
                self.tango_device.push_change_event(
                    f"hardware_port_{port_number}_temperature",
                    self._qsfp_temperature[f"{port_number}"],
                )
                self.tango_device.push_archive_event(
                    f"hardware_port_{port_number}_temperature",
                    self._qsfp_temperature[f"{port_number}"],
                )
                self.tango_device.push_change_event(
                    f"hardware_port_{port_number}_txpower",
                    self._qsfp_tx_power[f"{port_number}"],
                )
                self.tango_device.push_archive_event(
                    f"hardware_port_{port_number}_txpower",
                    self._qsfp_tx_power[f"{port_number}"],
                )
                self.tango_device.push_change_event(
                    f"hardware_port_{port_number}_rxpower",
                    self._qsfp_rx_power[f"{port_number}"],
                )
                self.tango_device.push_archive_event(
                    f"hardware_port_{port_number}_rxpower",
                    self._qsfp_rx_power[f"{port_number}"],
                )
            self.tango_device.push_change_event(
                "unit_serial", self._serial_number
            )
            self.tango_device.push_archive_event(
                "unit_serial", self._serial_number
            )
            self.tango_device.push_change_event(
                "hardware_motherboard_temperature",
                self._average_temperature,
            )
            self.tango_device.push_archive_event(
                "hardware_motherboard_temperature",
                self._average_temperature,
            )
            self.tango_device.push_change_event(
                "hardware_tofino_temperature",
                self._tofino_temperature,
            )
            self.tango_device.push_archive_event(
                "hardware_tofino_temperature",
                self._tofino_temperature,
            )
            self.tango_device.update_information_from_bfshell(
                self._qsfp_serials,
                self._qsfp_temperature,
                self._qsfp_rx_power,
                self._qsfp_tx_power,
                self._tofino_temperature,
                self._average_temperature,
                self._serial_number,
            )
            sleep(5)

    def update_information_from_bfshell(self, grpc_address: str):
        """Get information from the BFshell."""

        while True:
            try:
                with grpc.insecure_channel(grpc_address) as channel:
                    stub = (
                        ska_low_cbf_conn.bfshell.bfshell_pb2_grpc.BFshellStub(
                            channel
                        )
                    )
                    response_serials = stub.get_qsfp_serials(
                        ska_low_cbf_conn.bfshell.bfshell_pb2.Empty()
                    )
                    new_serials = False
                    if self._qsfp_serials != json.loads(
                        response_serials.message
                    ):
                        self._qsfp_serials = deepcopy(
                            json.loads(response_serials.message)
                        )
                        new_serials = True

                    # print("==== List of QFSP serial ====")
                    # print("BFShellReader client received: " + response.message)
                    # print("==== Temperature of QFSPs ====")
                    for port_number, serial in json.loads(
                        response_serials.message
                    ).items():
                        response_temp = stub.get_qsfp_temp_power(
                            ska_low_cbf_conn.bfshell.bfshell_pb2.ShellRequest(
                                name=f"{port_number}"
                            )
                        )
                        if new_serials:
                            self.tango_device.push_change_event(
                                f"diagnostics_port_{port_number}_serial",
                                serial,
                            )
                            self.tango_device.push_archive_event(
                                f"diagnostics_port_{port_number}_serial",
                                serial,
                            )
                        response_temp_dict = json.loads(response_temp.message)
                        if response_temp_dict[port_number] == {}:
                            if self._qsfp_temperature[port_number] != 0:
                                self._qsfp_temperature[port_number] = 0
                                self._qsfp_tx_power[
                                    port_number
                                ] = "-40.0,-40.0,-40.0,-40.0"
                                self._qsfp_rx_power[
                                    port_number
                                ] = "-40.0,-40.0,-40.0,-40.0"
                                self.tango_device.push_change_event(
                                    f"hardware_port_{port_number}_temperature",
                                    self._qsfp_temperature[port_number],
                                )
                                self.tango_device.push_archive_event(
                                    f"hardware_port_{port_number}_temperature",
                                    self._qsfp_temperature[port_number],
                                )
                                self.tango_device.push_change_event(
                                    f"hardware_port_{port_number}_txpower",
                                    self._qsfp_tx_power[port_number],
                                )
                                self.tango_device.push_archive_event(
                                    f"hardware_port_{port_number}_txpower",
                                    self._qsfp_tx_power[port_number],
                                )
                                self.tango_device.push_change_event(
                                    f"hardware_port_{port_number}_rxpower",
                                    self._qsfp_rx_power[port_number],
                                )
                                self.tango_device.push_archive_event(
                                    f"hardware_port_{port_number}_rxpower",
                                    self._qsfp_rx_power[port_number],
                                )
                        else:
                            if self._qsfp_temperature[port_number] != float(
                                response_temp_dict[port_number]["temperature"]
                            ):
                                self._qsfp_temperature[port_number] = int(
                                    np.round(
                                        float(
                                            response_temp_dict[port_number][
                                                "temperature"
                                            ]
                                        )
                                    )
                                )
                                self.tango_device.push_change_event(
                                    f"hardware_port_{port_number}_temperature",
                                    self._qsfp_temperature[port_number],
                                )
                                self.tango_device.push_archive_event(
                                    f"hardware_port_{port_number}_temperature",
                                    self._qsfp_temperature[port_number],
                                )
                            tx_power = (
                                f"{response_temp_dict[port_number]['channel_1_tx'].split('dBm')[0]}, "
                                + f"{response_temp_dict[port_number]['channel_2_tx'].split('dBm')[0]}, "
                                + f"{response_temp_dict[port_number]['channel_3_tx'].split('dBm')[0]}, "
                                + f"{response_temp_dict[port_number]['channel_4_tx'].split('dBm')[0]} "
                            )
                            if tx_power != "-inf, -inf, -inf, -inf":
                                float_tx_power = [
                                    float(tx_pow)
                                    for tx_pow in tx_power.split(", ")
                                ]
                                tx_power = (
                                    f"{float_tx_power[0]:.1f}, "
                                    + f"{float_tx_power[1]:.1f}, "
                                    + f"{float_tx_power[2]:.1f}, "
                                    + f"{float_tx_power[3]:.1f}"
                                )

                            if self._qsfp_tx_power[port_number] != tx_power:
                                self._qsfp_tx_power[port_number] = tx_power
                                self.tango_device.push_archive_event(
                                    f"hardware_port_{port_number}_txpower",
                                    self._qsfp_tx_power[port_number],
                                )
                                self.tango_device.push_change_event(
                                    f"hardware_port_{port_number}_txpower",
                                    self._qsfp_tx_power[port_number],
                                )
                            rx_power = (
                                f"{response_temp_dict[port_number]['channel_1_rx'].split('dBm')[0]}, "
                                + f"{response_temp_dict[port_number]['channel_2_rx'].split('dBm')[0]}, "
                                + f"{response_temp_dict[port_number]['channel_3_rx'].split('dBm')[0]}, "
                                + f"{response_temp_dict[port_number]['channel_4_rx'].split('dBm')[0]} "
                            )
                            if rx_power != "-inf, -inf, -inf, -inf":
                                float_rx_power = [
                                    float(rx_pow)
                                    for rx_pow in rx_power.split(", ")
                                ]
                                rx_power = (
                                    f"{float_rx_power[0]:.1f}, "
                                    + f"{float_rx_power[1]:.1f}, "
                                    + f"{float_rx_power[2]:.1f}, "
                                    + f"{float_rx_power[3]:.1f}"
                                )
                            if self._qsfp_rx_power[port_number] != rx_power:
                                self._qsfp_rx_power[port_number] = rx_power
                                self.tango_device.push_change_event(
                                    f"hardware_port_{port_number}_rxpower",
                                    self._qsfp_rx_power[port_number],
                                )
                                self.tango_device.push_archive_event(
                                    f"hardware_port_{port_number}_rxpower",
                                    self._qsfp_rx_power[port_number],
                                )

                    resp_serial = stub.get_serial(
                        ska_low_cbf_conn.bfshell.bfshell_pb2.Empty()
                    )
                    serial_number = json.loads(resp_serial.message)["serial"]
                    if self._serial_number != serial_number:
                        self._serial_number = serial_number
                        self.tango_device.push_change_event(
                            "unit_serial", self._serial_number
                        )
                        self.tango_device.push_archive_event(
                            "unit_serial", self._serial_number
                        )

                    response_global_temp = stub.get_general_temp(
                        ska_low_cbf_conn.bfshell.bfshell_pb2.Empty()
                    )
                    if response_global_temp != {}:
                        average_temp = 0.0
                        for sensor_name, temperature in json.loads(
                            response_global_temp.message
                        ).items():
                            if sensor_name != "tofino_board_sensor":
                                average_temp += float(temperature)
                            else:
                                if self._tofino_temperature != float(
                                    temperature
                                ):
                                    self._tofino_temperature = float(
                                        temperature
                                    )
                                    self.tango_device.push_change_event(
                                        "hardware_tofino_temperature",
                                        self._tofino_temperature,
                                    )
                                    self.tango_device.push_archive_event(
                                        "hardware_tofino_temperature",
                                        self._tofino_temperature,
                                    )
                        if self._average_temperature != average_temp / 6:
                            self._average_temperature = average_temp / 6
                            self.tango_device.push_change_event(
                                "hardware_motherboard_temperature",
                                self._average_temperature,
                            )
                            self.tango_device.push_archive_event(
                                "hardware_motherboard_temperature",
                                self._average_temperature,
                            )
                    # print("==== Overall Temperature ====")
                    # print("BFShellReader client received: " + response.message)

            except Exception as exception:
                self.logger.info(
                    f"error {exception} while retrieving information from BFShell ",
                )
            self.tango_device.update_information_from_bfshell(
                self._qsfp_serials,
                self._qsfp_temperature,
                self._qsfp_rx_power,
                self._qsfp_tx_power,
                self._tofino_temperature,
                self._average_temperature,
                self._serial_number,
            )
            sleep(5)

    def advanced_sps_telemetry(self):
        """Get new report from the P4 switch about SPS traffic."""

        while True:
            if self.connector is not None:
                sps_rates = self.connector.get_spead_throughput()
                if self._sps_rates != sps_rates:
                    self.tango_device.push_change_event(
                        "spsRates", json.dumps(sps_rates)
                    )
                    self.tango_device.push_archive_event(
                        "spsRates", json.dumps(sps_rates)
                    )
                    # self.logger.info("New Health Status published")
                    self._sps_rates = deepcopy(sps_rates)
                    self.tango_device.advanced_sps_telemetry(self._sps_rates)

            sleep(5)
