import argparse
import ctypes as ct
import os
import socket
import struct
import sys
import time
from ast import literal_eval
from ctypes import *
from threading import Thread

from bcc import BPF
from prometheus_client import Counter, Gauge, Summary, start_http_server

# from ska_low_cbf_conn.spead_connector import SpeadConnector


class TelemetryEBPF:
    def __init__(
        self, interface: str, interval: int, connector, with_prometheus=False
    ):
        """
        Create an interface to P4 switch
        :param interface: the interface to listen to
        :param connector: connector to report throughput to
        :return:
        """
        self.interface = interface
        self.interval = interval
        self.connector = connector
        self.bpf_text = """

#include <uapi/linux/ptrace.h>
#include <net/sock.h>
#include <bcc/proto.h>
#include <linux/bpf.h>

#define IP_TCP 6
#define IP_UDP 17
#define IP_ICMP 1
#define ETH_HLEN 14

BPF_PERF_OUTPUT(skb_events);

BPF_HASH(packet_throughput, u64, u64, 256); 

BPF_HASH(packet_cnt, u64, u64, 256);

struct spead_t {
  u16 freq;
  u16 sub;
  u8 beam;
  u32 size; 
  u32 current_tstamp_telemetry;
  u32 previous_tstamp_telemetry;
} BPF_PACKET_HEADER;

int packet_monitor(struct __sk_buff *skb) {
    u8 *cursor = 0;
    u32 size, current_tstamp_telemetry, previous_tstamp_telemetry, elapsed;
    u16 freq, sub;
    u8 beam;
    u64* count = 0;
    u64 one = 1.0;
    u64 pass_key = 0;
    u64 pass_key_2 = 0;
    u64 pass_value = 0;
    u64 pass_value_bytes = 0;
    u64* count_bytes = 0;
    
    struct ethernet_t *ethernet = cursor_advance(cursor, sizeof(*ethernet));

    struct ip_t *ip = cursor_advance(cursor, sizeof(*ip));
    if (ip -> nextp != IP_UDP) 
    {
        return 0; 
    }
    struct udp_t *udp = cursor_advance(cursor, sizeof(*udp));
    if (udp -> dport != 4660)
    {
        return 0;
    }
    struct spead_t *spead = cursor_advance(cursor, sizeof(*spead));
    freq = spead -> freq;
    sub = spead -> sub;
    beam = spead -> beam;
    size = spead -> size;
    current_tstamp_telemetry = spead -> current_tstamp_telemetry;
    previous_tstamp_telemetry = spead -> previous_tstamp_telemetry;

    if (previous_tstamp_telemetry > current_tstamp_telemetry)
    {
        elapsed = 4294967296-previous_tstamp_telemetry+current_tstamp_telemetry;
    }
    else
    {
        elapsed = current_tstamp_telemetry - previous_tstamp_telemetry;
    }
    pass_key = freq;
    pass_key = pass_key << 16;
    pass_key = pass_key + sub;
    pass_key = pass_key << 32;
    pass_key = pass_key + beam;
    pass_value = size; 
    pass_value = pass_value << 32;
    pass_value = pass_value + elapsed;
    pass_value_bytes = size;
    pass_key_2 = freq;
    pass_key_2 = pass_key_2 << 16;
    pass_key_2 = pass_key_2 + sub;
    pass_key_2 = pass_key_2 << 32;
    pass_key_2 = pass_key_2 + beam;

    
    

    count = packet_throughput.lookup(&pass_key); 
    if (count)  // check if this map exists
    { 
            *count = pass_value;
    }
    else        // if the map for the key doesn't exist, create one
        {
            packet_throughput.update(&pass_key, &one);
        }
    count_bytes = packet_cnt.lookup(&pass_key_2); 
    if (count_bytes)  // check if this map exists
    { 
            *count_bytes += size;
    }
    else        // if the map for the key doesn't exist, create one
        {
            packet_cnt.update(&pass_key_2, &pass_value_bytes);
        }
    return -1;
}

"""
        self.bpf = BPF(text=self.bpf_text)
        self.function_skb_matching = self.bpf.load_func(
            "packet_monitor", BPF.SOCKET_FILTER
        )
        BPF.attach_raw_socket(self.function_skb_matching, self.interface)
        self.packet_cnt = self.bpf.get_table("packet_cnt")
        self.packet_throughput = self.bpf.get_table("packet_throughput")
        self.spead_throughput = {}
        self.spead_byte_counts = {}
        self.with_prometheus = with_prometheus
        if with_prometheus:
            self.spead_throughput_prometheus = Gauge(
                "throughput_spead",
                "Throughput of SPEAD traffic",
                ["Frequency", "Sub", "Beam"],
            )
            self.spead_bytes_prometheus = Gauge(
                "bytes_spead",
                "Bytes received of SPEAD traffic",
                ["Frequency", "Sub", "Beam"],
            )
            start_http_server(8000)

    def get_thoughput_spead(self):
        try:
            while True:
                time.sleep(self.interval)
                packet_cnt_output = self.packet_throughput.items()
                self.packet_throughput.clear()  # delete map entires after printing output. confiremd it deletes values and keys too
                output_len = len(packet_cnt_output)
                # print("output length {}".format(output_len))
                for key, value in self.spead_throughput.items():
                    self.spead_throughput[key] = 0
                for i in range(0, output_len):
                    bytes_key = packet_cnt_output[i][0].value.to_bytes(
                        8, "big"
                    )
                    byte_array = bytearray(bytes_key)
                    byte_and_time = packet_cnt_output[i][1].value.to_bytes(
                        8, "big"
                    )
                    byte_and_time_array = bytearray(byte_and_time)
                    throughput = (
                        int.from_bytes(
                            byte_and_time_array[:4], byteorder="big"
                        )
                        * 8
                        * 1000000000
                        / int.from_bytes(
                            byte_and_time_array[4:8], byteorder="big"
                        )
                    )
                    if self.with_prometheus:
                        self.spead_throughput_prometheus.labels(
                            int.from_bytes(byte_array[:2], byteorder="big"),
                            byte_array[3],
                            byte_array[7],
                        ).set(throughput)
                    self.spead_throughput[
                        "{} {} {}".format(
                            int.from_bytes(byte_array[:2], byteorder="big"),
                            byte_array[3],
                            byte_array[7],
                        )
                    ] = throughput
                self.connector.set_spead_throughput(self.spead_throughput)
                for key in list(self.spead_throughput.keys()):
                    if self.spead_throughput[key] == 0:
                        if self.with_prometheus:
                            key_split = key.split(" ")
                            self.spead_throughput_prometheus.labels(
                                key_split[0], key_split[1], key_split[2]
                            ).set(0)
                        del self.spead_throughput[key]

                packet_cnt_output_bytes = self.packet_cnt.items()
                output_len_bytes = len(packet_cnt_output_bytes)
                for i in range(0, output_len_bytes):
                    bytes_key = packet_cnt_output_bytes[i][0].value.to_bytes(
                        8, "big"
                    )
                    byte_array = bytearray(bytes_key)
                    byte_and_time = packet_cnt_output_bytes[i][
                        1
                    ].value.to_bytes(8, "big")
                    byte_and_time_array = bytearray(byte_and_time)
                    if self.with_prometheus:
                        self.spead_bytes_prometheus.labels(
                            int.from_bytes(byte_array[:2], byteorder="big"),
                            byte_array[3],
                            byte_array[7],
                        ).set(
                            int.from_bytes(
                                byte_and_time_array[:8], byteorder="big"
                            )
                        )
                    self.spead_byte_counts[
                        "{} {} {}".format(
                            int.from_bytes(byte_array[:2], byteorder="big"),
                            byte_array[3],
                            byte_array[7],
                        )
                    ] = int.from_bytes(
                        byte_and_time_array[:8], byteorder="big"
                    )

                self.connector.set_spead_bytes(self.spead_byte_counts)
        except KeyboardInterrupt:
            sys.stdout.close()
            pass


def launch_telemetry():
    argparser = argparse.ArgumentParser(
        description="Perentie eBPF Telemetry for Spead."
    )
    argparser.add_argument(
        "--interface",
        type=str,
        default="enp4s0f0",
        help="Interface to listen to. Default: enp4s0f0",
    )
    argparser.add_argument(
        "--interval",
        type=int,
        default=1,
        help="Time between checking for new data. Default: 1 second",
    )
    args = argparser.parse_args()
    telemetry = TelemetryEBPF(args.interface, args.interval, 1)
    telemetry_thread = Thread(target=telemetry.get_thoughput_spead)
    telemetry_thread.start()


def main():
    launch_telemetry()


if __name__ == "__main__":
    main()
