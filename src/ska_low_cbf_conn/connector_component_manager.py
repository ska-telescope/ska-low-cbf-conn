# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Connector Component Manager
"Business Logic" for communicating with the hardware
"""

import logging

from ska_tango_base.base import BaseComponentManager
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import CommunicationStatus, PowerState


class ConnectorComponentManager(BaseComponentManager):
    def __init__(
        self,
        logger: logging.Logger,
        communication_state_callback,  #: Callable[[CommunicationStatus]],
        component_state_callback,  #: Callable,
    ):
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=PowerState.UNKNOWN,
            fault=None,
        )

    def start_communicating(self):
        # Intended to start comms to a hardware device
        # Called when adminMode is changed to ONLINE
        # FIXME - this isn't doing anything yet...
        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        self._update_component_state(power=PowerState.ON, fault=False)

    def stop_communicating(self):
        # Intended to stop comms to a hardware device
        # Called when adminMode changed to OFFLINE
        # FIXME - this isn't doing anything yet...
        self._update_component_state(power=PowerState.OFF, fault=False)
        self._update_communication_state(CommunicationStatus.DISABLED)

    def off(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored OFF"
        self.logger.error(message)
        return ResultCode.REJECTED, message

    def standby(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored STANDBY"
        self.logger.error(message)
        return ResultCode.REJECTED, message

    def on(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored ON"
        self.logger.error(message)
        return ResultCode.REJECTED, message

    def reset(self, _):  # 2nd arg is "task_callback: Callable"
        message = "Ignored RESET"
        self.logger.error(message)
        return ResultCode.REJECTED, message
