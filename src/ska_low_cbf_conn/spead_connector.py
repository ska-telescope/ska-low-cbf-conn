# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import argparse
import asyncio
import binascii
import glob
import ipaddress
import json
import logging
import os
import re
import signal
import socket
import sys
from concurrent import futures
from copy import copy, deepcopy
from enum import IntEnum, unique
from threading import Thread

import bfrt_grpc.client as gc
import yaml
from ska_low_cbf_net.connector import (
    CommandError,
    Connector,
    _port,
    _protocol,
    validate_ip,
)

from ska_low_cbf_conn.arp_resolver import ARPResolver
from ska_low_cbf_conn.ports import PortTof2
from ska_low_cbf_conn.psr import Psr
from ska_low_cbf_conn.ptp import PTP
from ska_low_cbf_conn.sdp_ip import SDP_IP
from ska_low_cbf_conn.sdp_mac import SDP_MAC
from ska_low_cbf_conn.spead import Spead
from ska_low_cbf_conn.spead_correlator import SpeadCorrelator
from ska_low_cbf_conn.telemetry_ebpf import TelemetryEBPF

if "SDE_INSTALL" in os.environ and os.path.exists(os.environ["SDE_INSTALL"]):
    bfrt_location = "{}/lib/python*/site-packages/tofino".format(
        os.environ["SDE_INSTALL"]
    )
    sys.path.append(glob.glob(bfrt_location)[0])
    bfrt_loc = "{}/lib/python*/site-packages/tofino/bfrt_grpc".format(
        os.environ["SDE_INSTALL"]
    )
    sys.path.append(glob.glob(bfrt_loc)[0])
else:
    mockup_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "mockup"
    )
    sys.path.append(mockup_path)


mac_address_regex = re.compile(":".join(["[0-9a-fA-F]{2}"] * 6))
front_panel_regex = re.compile("([0-9]+)/([0-9]+)$")


class SpeadConnector(Connector):
    """Interface for p4 in-network processor with SPEAD protocol"""

    monitor_channels = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
    """Frequency channels monitored for packet/byte loss"""

    @unique
    class Protocol(IntEnum):
        UNKNOWN = 0
        ARP = 1
        ICMP = 2
        IP_OTHER = 3
        UDP_OTHER = 4
        SPEAD = 5
        PSR = 6

    def __init__(
        self, program: str, bfrt_ip: str, bfrt_port: str, client_id: int = 0
    ):
        """
        Create an interface to P4 switch with SPEAD & PSR protocols
        :param program:
        :param bfrt_ip:
        :param bfrt_port:
        :return:
        """
        super().__init__(
            program=program,
            bfrt_ip=bfrt_ip,
            bfrt_port=bfrt_port,
            client_id=client_id,
        )
        self.protocol_names = {_.value: _.name for _ in self.Protocol}
        self.psr = None
        self.spead = None
        # create empty byte & packet tables for our protocols
        self._psr_bytes = {}
        self._psr_packets = {}
        self._spead_bytes = {}
        self._spead_throughput = {}
        self._spead_bytes_lost = {_: 0 for _ in self.monitor_channels}
        self._spead_packets = {}
        self._spead_packets_lost = {_: 0 for _ in self.monitor_channels}
        self._ip_address = bfrt_ip
        self._tcp_port = bfrt_port
        self._program_name = program
        try:
            # spead manipulation
            self.spead = Spead(self.target, gc, self.bfrt_info)
            # Ports table
            self.ports = PortTof2(self.target, gc, self.bfrt_info)
            # psr manipulation
            self.psr = Psr(self.target, gc, self.bfrt_info)
            # spead manipulation
            self.spead_multiplier = SpeadCorrelator(
                self.target, gc, self.bfrt_info
            )
            # SDP configuration
            self.sdp_ip = SDP_IP(self.target, gc, self.bfrt_info)
            self.sdp_mac = SDP_MAC(self.target, gc, self.bfrt_info)
            # PTP configuration
            self.ptp = PTP(self.target, gc, self.bfrt_info)
            # ARP resolver
            self.arp_resolver = ARPResolver("net1")
            self.send_thread = Thread(
                target=self.arp_resolver.send_arp_request
            )
            self.send_thread.start()
            self.resolver_thread = Thread(
                target=self.arp_resolver.receive_arp_reply
            )
            self.resolver_thread.start()
            # Sub Station table, used only in engineering and commissioning
            self.sub_station = self.bfrt_info.table_get("sub_station_table")
            # configuring ARP return table
            self.arp_table = self.bfrt_info.table_get("arp_table")
            try:
                self.arp_table.entry_add(
                    self.target,
                    [
                        self.arp_table.make_key(
                            [
                                gc.KeyTuple(
                                    "target_ip",
                                    int(ipaddress.ip_address("192.168.1.101")),
                                )
                            ]
                        )
                    ],
                    [
                        self.arp_table.make_data(
                            [
                                gc.DataTuple("dest_port", 5),
                                gc.DataTuple(
                                    "dst_ip_addr",
                                    int(ipaddress.ip_address("192.168.1.2")),
                                ),
                                gc.DataTuple(
                                    "src_ip_addr",
                                    int(ipaddress.ip_address("192.168.1.1")),
                                ),
                            ],
                            "send_reply_from_sdp",
                        )
                    ],
                )
            except Exception as e:
                self.log.info("ARP table entries already configured")
            # configuration of the two multicast session
            # multicast session 1 for PTP and 2 for ARP to SDP
            try:
                self.create_muticast_session(1)

                self.create_muticast_session(2)
                self.configure_multicast_session(
                    1,
                    1,
                    1,
                    1,
                    int("0x1FFF", 16),
                    int("0x1EEE", 16),
                )
                self.configure_multicast_session(
                    2,
                    513,
                    2,
                    2,
                    int("0x1FFF", 16),
                    int("0x1EEE", 16),
                )
            except Exception as e:
                self.log.info("Multicast sessions 1 and 2 already exists")
            # Configuration of the mirroring for the advanced telemetry
            self.mirror_cfg_table = self.bfrt_info.table_get("$mirror.cfg")
            mirror_cfg_bfrt_key = self.mirror_cfg_table.make_key(
                [gc.KeyTuple("$sid", 27)]
            )
            mirror_cfg_bfrt_data = self.mirror_cfg_table.make_data(
                [
                    gc.DataTuple("$direction", str_val="INGRESS"),
                    gc.DataTuple(
                        "$ucast_egress_port", 5
                    ),  # port 33/3 on the tofino
                    gc.DataTuple("$ucast_egress_port_valid", bool_val=True),
                    gc.DataTuple("$session_enable", bool_val=True),
                    gc.DataTuple("$max_pkt_len", 128),
                ],
                "$normal",
            )
            # entries_from_switch = self.mirror_cfg_table.entry_get(self.target)
            try:
                self.mirror_cfg_table.entry_add(
                    self.target, [mirror_cfg_bfrt_key], [mirror_cfg_bfrt_data]
                )
            except gc.BfruntimeReadWriteRpcException as be:
                self.log.info("Mirror entry for telemetry already exists")
            except Exception as e:
                self.log.exception(e)
            self.telemetry_egress_table = self.bfrt_info.table_get(
                "telemetry_table"
            )
            telemetry_egress_key = self.telemetry_egress_table.make_key(
                [gc.KeyTuple("pkt_type", 2)]
            )
            mac_address_from = int.from_bytes(
                binascii.unhexlify("aa:bb:cc:ee:dd:ff".replace(":", "")),
                byteorder="big",
                signed=False,
            )
            mac_address_broadcast = int.from_bytes(
                binascii.unhexlify("ff:ff:ff:ff:ff:ff".replace(":", "")),
                byteorder="big",
                signed=False,
            )
            telemetry_egress_data = self.telemetry_egress_table.make_data(
                [
                    gc.DataTuple(
                        "src_addr", int(ipaddress.ip_address("192.168.1.100"))
                    ),
                    gc.DataTuple(
                        "dst_addr", int(ipaddress.ip_address("192.168.1.101"))
                    ),
                    gc.DataTuple("hw_src_addr", mac_address_from),
                    gc.DataTuple("hw_dst_addr", mac_address_broadcast),
                ],
                "set_telemetry_header",
            )
            # entries_from_switch = self.mirror_cfg_table.entry_get(self.target)
            try:
                self.telemetry_egress_table.entry_add(
                    self.target,
                    [telemetry_egress_key],
                    [telemetry_egress_data],
                )
            except gc.BfruntimeReadWriteRpcException as be:
                self.log.info("Telemetry entry already exists")
            except Exception as e:
                self.log.exception(e)

        except KeyboardInterrupt:
            self.critical_error("Stopping controller.")
        except Exception as e:
            self.log.exception(e)
            self.critical_error("Unexpected error. Stopping controller.")

    # SPEAD protocol methods
    def get_spead_table_entries(self):
        """Return entries of SPEAD table."""
        entries = []
        try:
            for match, dev_port in self.spead.get_entries().items():
                success2, fp_port, fp_lane = self.ports.get_fp_port(dev_port)
                if not success2:
                    raise CommandError("Port number invalid")
                else:
                    entry = {
                        "Frequency": "{}".format(match.split()[0]),
                        "Beam": "{}".format(match.split()[1]),
                        "Sub_array": "{}".format(match.split()[2]),
                        "port": "{}/{}".format(fp_port, fp_lane),
                    }
                    entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def add_spead_table_entry(
        self, frequency: int, beam: int, sub_array: int, e_port
    ):
        """Add SPEAD Table entry. The SPEAD table match a given frequency to an egress port.
        This function create a entry and take two arguments, first the frequency then egress port.
        """

        lane_out = 0
        out_re_match = e_port
        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out <= 65):
                raise CommandError("Egress Port number invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError("Invalid Egress lane")

        success, dev_port = self.ports.get_dev_port(port_out, lane_out)
        match = [frequency, beam, sub_array]
        self.spead.add_entry(match, dev_port)

    def update_spead_table_entry(
        self, frequency: int, beam: int, sub_array: int, e_port
    ):
        """Add SPEAD Table entry. The SPEAD table match a given frequency to an egress port.
        This function create a entry and take two arguments, first the frequency then egress port.
        """

        lane_out = 0
        out_re_match = e_port
        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out <= 65):
                raise CommandError("Egress Port number invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError("Invalid Egress lane")

        success, dev_port = self.ports.get_dev_port(port_out, lane_out)
        match = [frequency, beam, sub_array]
        self.spead.update_entry(match, dev_port)

    def get_spead_table_counters(self):
        """Return counters of SPEAD table."""
        entries = []
        try:
            for match, counters in self.spead.get_counters().items():
                entry = {
                    "Frequency": "{}".format(match.split()[0]),
                    "Beam": "{}".format(match.split()[1]),
                    "Sub_array": "{}".format(match.split()[2]),
                    "Pkts": "{}".format(counters["Pkts"]),
                    "Bytes": "{}".format(counters["Bytes"]),
                }
                entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def remove_spead_table_entry(
        self, frequency: int, beam: int, sub_array: int
    ):
        """Remove SPEAD Multiplier Table entry. The SPEAD table match a given frequency beam and sub_arrays to
        an egress port for beamformer and one for correlator.
        This function remove an entry
        """
        match = [frequency, beam, sub_array]
        self.spead.remove_entry(match)

    def reset_spead_counters(self):
        self.spead.reset_counters()

    def clear_spead_table(self):
        self.spead.clear()

    # SPEAD with multiplier protocol methods
    def get_spead_multiplier_table_entries(self):
        """Return entries of SPEAD table."""
        entries = []
        try:
            for match, session in self.spead_multiplier.get_entries().items():
                entry = {
                    "Frequency": "{}".format(match.split()[0]),
                    "Beam": "{}".format(match.split()[1]),
                    "Sub_array": "{}".format(match.split()[2]),
                    "session": "{}".format(session),
                }
                entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def add_spead_multiplier_table_entry(
        self, frequency: int, beam: int, sub_array: int, list_of_ports
    ):
        """Add SPEAD Multiplier Table entry. The SPEAD table match a given frequency beam and sub_arrays to an egress port for beamformer and one for correlator.
        This function create a entry and take two arguments, first the frequency then egress port.
        """
        dest_ports = []
        for port in list_of_ports:
            lane_out = 0
            out_re_match = port
            if out_re_match and out_re_match.group(1):
                port_out = int(out_re_match.group(1))
                if not (1 <= port_out <= 65):
                    raise CommandError("Egress Port number invalid")
            else:
                raise CommandError("Egress Port number invalid")

            if out_re_match.group(2):
                lane_out = int(out_re_match.group(2))
                if lane_out not in range(0, 4):
                    raise CommandError("Invalid Egress lane")
            success, dev_port = self.ports.get_dev_port(port_out, lane_out)
            dest_ports.append(dev_port)
        match = [frequency, beam, sub_array]
        sessions_and_ports = self.multiplier.get_sessions_and_ports()
        session_id = 0
        for session, ports in sessions_and_ports.items():
            if set(ports) == set(dest_ports):
                session_id = int(session)
        if session_id == 0:
            # need to create session and everything here
            session_id = len(sessions_and_ports) + 1
            self.create_muticast_session(session_id)
            self.configure_multicast_session(
                session_id,
                session_id * 260,
                session_id,
                session_id,
                int("0x1FFF", 16),
                int("0x1EEE", 16),
            )
            for port in list_of_ports:
                self.add_port_to_multicast_session(
                    session_id, session_id * 260, port
                )

        self.spead_multiplier.add_entry(match, session_id)

    def update_spead_multiplier_table_entry(
        self, frequency: int, beam: int, sub_array: int, list_of_ports
    ):
        """Add SPEAD Multiplier Table entry. The SPEAD table match a given frequency to an egress port.
        This function create a entry and take two arguments, first the frequency then egress port.
        """
        dest_ports = []
        for port in list_of_ports:
            lane_out = 0
            out_re_match = port
            if out_re_match and out_re_match.group(1):
                port_out = int(out_re_match.group(1))
                if not (1 <= port_out <= 65):
                    raise CommandError("Egress Port number invalid")
            else:
                raise CommandError("Egress Port number invalid")

            if out_re_match.group(2):
                lane_out = int(out_re_match.group(2))
                if lane_out not in range(0, 4):
                    raise CommandError("Invalid Egress lane")
            success, dev_port = self.ports.get_dev_port(port_out, lane_out)
            dest_ports.append(dev_port)
        match = [frequency, beam, sub_array]
        sessions_and_ports = self.multiplier.get_sessions_and_ports()
        session_id = 0
        for session, ports in sessions_and_ports.items():
            if set(ports) == set(dest_ports):
                session_id = int(session)
        if session_id == 0:
            # need to create session and everything here
            session_id = len(sessions_and_ports) + 1
            self.create_muticast_session(session_id)
            self.configure_multicast_session(
                session_id,
                session_id * 260,
                session_id,
                session_id,
                int("0x1FFF", 16),
                int("0x1EEE", 16),
            )
            for port in list_of_ports:
                self.add_port_to_multicast_session(
                    session_id, session_id * 260, port
                )
        self.spead_multiplier.update_entry(match, session_id)

    def get_spead_multiplier_table_counters(self):
        """Return counters of SPEAD table."""
        entries = []
        try:
            for (
                match,
                counters,
            ) in self.spead_multiplier.get_counters().items():
                entry = {
                    "Frequency": "{}".format(match.split()[0]),
                    "Beam": "{}".format(match.split()[1]),
                    "Sub_array": "{}".format(match.split()[2]),
                    "Pkts": "{}".format(counters["Pkts"]),
                    "Bytes": "{}".format(counters["Bytes"]),
                }
                entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def remove_spead_multiplier_table_entry(
        self, frequency: int, beam: int, sub_array: int
    ):
        """Remove SPEAD Multiplier Table entry. The SPEAD table match a given frequency beam and sub_arrays to
        an egress port for beamformer and one for correlator.
        This function remove an entry
        """
        match = [frequency, beam, sub_array]
        self.spead_multiplier.remove_entry(match)

    def reset_spead_multiplier_counters(self):
        self.spead_multiplier.reset_counters()

    def clear_spead_multiplier_table(self):
        self.spead_multiplier.clear()

    # PSR protocol methods
    def add_psr_table_entry(
        self, beam: int, out_re_match: str, udp_dest: int = 9510
    ):
        """Add PSR Table entry. The PSR table match a given beam to an egress port.
        This function create a entry and take two arguments, beam and egress port.
        """
        # port_out = e_port
        lane_out = 0
        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out and port_out <= 65):
                raise CommandError("Egress Port number invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError("Invalid Egress lane")

        success, dev_port = self.ports.get_dev_port(port_out, lane_out)
        match = beam
        action = [dev_port, udp_dest]
        self.psr.add_entry(match, action)

    def update_psr_table_entry(
        self, beam: int, out_re_match: str, udp_dest: int = 9510
    ):
        """Add SPEAD Table entry. The SPEAD table match a given frequency to an egress port.
        This function create a entry and take two arguments, first the frequency then egress port.
        """
        # port_out = e_port
        lane_out = 0
        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out and port_out <= 65):
                raise CommandError("Egress Port number invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError("Invalid Egress lane")

        success, dev_port = self.ports.get_dev_port(port_out, lane_out)
        match = beam
        action = [dev_port, udp_dest]
        self.psr.update_entry(match, action)

    def remove_psr_table_entry(self, beam: int):
        """Remove PSR Table entry. The PSR table match a given frequency beam
        This function remove an entry
        """
        match = beam
        self.psr.remove_entry(match)

    def get_psr_table_counters(self):
        """Return counters of PSR table."""
        entries = []
        try:
            for match, counters in self.psr.get_counters().items():
                entry = {
                    "Beam": "{}".format(match),
                    "Pkts": "{}".format(counters["Pkts"]),
                    "Bytes": "{}".format(counters["Bytes"]),
                }
                entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def get_psr_table_entries(self):
        """Return entries of PSR table."""
        entries = []
        try:
            for match, dev_port in self.psr.get_entries().items():
                success2, fp_port, fp_lane = self.ports.get_fp_port(
                    dev_port["dest_port"]
                )
                if not success2:
                    raise CommandError("Port number invalid")
                else:
                    entry = {
                        "Beam": "{}".format(match),
                        "port": "{}/{}".format(fp_port, fp_lane),
                        "UDP_port": dev_port["destination_udp_port"],
                    }
                    entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def reset_psr_counters(self):
        """Reset counters of the PSR table"""
        self.psr.reset_counters()

    def clear_psr_table(self):
        """Clear the PSR table"""
        self.psr.clear()

    def start_ebpf_telemetry_table(self, interface, interval):
        self.ebpf_telemetry = TelemetryEBPF(
            interface=interface,
            interval=interval,
            connector=self,
            with_prometheus=False,
        )
        telemetry_thread = Thread(
            target=self.ebpf_telemetry.get_thoughput_spead
        )
        telemetry_thread.start()

    def set_spead_throughput(self, throughput):
        """Setting the spead throughput from the ebpf filter"""
        self._spead_throughput = throughput

    def set_spead_bytes(self, byte_cnt):
        """Setting the spead byte counts from the ebpf filter"""
        self._spead_bytes = byte_cnt

    def get_spead_throughput(self):
        """Return the latest throughput for SPEAD traffic"""
        return self._spead_throughput

    def get_spead_bytes(self):
        """Return the latest byte count for SPEAD traffic"""
        return self._spead_bytes

    def get_arp_resolver_list(self):
        """Return the current list of ARP --> (IP-port)"""
        ip_address_to_mac_and_port = (
            self.arp_resolver.get_ip_addresses_to_mac_and_port()
        )
        separate_ip_address_to_mac_and_port = deepcopy(
            ip_address_to_mac_and_port
        )
        for key, value in ip_address_to_mac_and_port.items():
            success, ing_fp_port, ing_fp_lane = self.ports.get_fp_port(
                int(value["port"])
            )
            if success:
                separate_ip_address_to_mac_and_port[key][
                    "port"
                ] = f"{ing_fp_port}/{ing_fp_lane}"
            else:
                self.log.error(
                    "Error getting ingress port for: %s", value["port"]
                )
        return separate_ip_address_to_mac_and_port

    def add_ip_address_to_resolver(self, ip_address: str):
        try:
            self.arp_table.entry_add(
                self.target,
                [
                    self.arp_table.make_key(
                        [
                            gc.KeyTuple(
                                "target_ip",
                                int(ipaddress.ip_address(ip_address)),
                            )
                        ]
                    )
                ],
                [
                    self.arp_table.make_data(
                        [
                            gc.DataTuple("ifid", 2),
                        ],
                        "send_arp_to_sdp",
                    )
                ],
            )
        except Exception as e:
            self.log.exception(e)
        self.arp_resolver.add_ip_to_resolve(ip_address)

    def remove_ip_address_from_resolver(self, ip_address: str):
        try:
            self.del_arp_table_entry(ip_address)
        except Exception as e:
            self.log.exception(e)
        self.arp_resolver.remove_ip_to_resolve(ip_address)

    def configure_multicast_for_SDP(self, SDP_ports: list):
        """
        Adds SDP ports to the SDP multicast group
        :param SDP_ports: physical switch ports that connect to SDP
        """
        for port in SDP_ports:
            self.add_port_to_multicast_session(2, 513, port)

    def remove_ports(self, port_to_delete: str = None):
        """Remove one specified port, or all active ports

        Keyword arguments:
            ports -- list of port to remove

        Returns:
            (success flag, list of ports or error message)
        """
        if port_to_delete:
            re_match = front_panel_regex.match(port_to_delete.strip())
            if re_match and re_match.group(1):
                port = int(re_match.group(1))
                if not (1 <= port and port <= 65):
                    raise CommandError("Port number invalid")
            else:
                raise CommandError("Port number invalid")
            if re_match.group(2):
                lane = int(re_match.group(2))
                if lane not in range(0, 4):
                    raise CommandError("Invalid lane")
            else:
                lane = 0
            success, error_msg = self.ports.remove_port(port, lane)
        else:
            success, stats = self.get_port_statistics()
            if success:
                for stat in stats:
                    re_match = front_panel_regex.match(
                        stat["$PORT_NAME"].strip()
                    )
                    if re_match and re_match.group(1):
                        port = int(re_match.group(1))
                        if not (1 <= port and port <= 65):
                            raise CommandError("Port number invalid")
                    else:
                        raise CommandError("Port number invalid")
                    if re_match.group(2):
                        lane = int(re_match.group(2))
                        if lane not in range(0, 4):
                            raise CommandError("Invalid lane")
                    else:
                        lane = 0
                    success, error_msg = self.ports.remove_port(port, lane)

    def get_ptp_table_entries(self):
        """
        Retrieve all PTP table entries
        """
        entries = []
        try:
            for ingress_port, dev_port in self.ptp.get_entries().items():
                success, ing_fp_port, ing_fp_lane = self.ports.get_fp_port(
                    ingress_port
                )
                success2, fp_port, fp_lane = self.ports.get_fp_port(dev_port)
                if not success or not success2:
                    raise CommandError("Port number invalid")
                else:
                    entry = {
                        "ingress port": "{}/{}".format(
                            ing_fp_port, ing_fp_lane
                        ),
                        "port": "{}/{}".format(fp_port, fp_lane),
                    }
                    entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def add_ptp_table_entry(self, in_re_match, out_re_match):
        """Add PTP Table entry. The PTP table match a given ingress port to an egress port for PTP traffic.
        This function creates an entry and take two arguments, first the ingress then egress port.
        This function aims for the traffic between Alveos to the PTP clock.
        """
        lane_in = None
        lane_out = None
        if in_re_match and in_re_match.group(1):
            port_in = int(in_re_match.group(1))
            if not (1 <= port_in and port_in <= 65):
                raise CommandError(f"Ingress Port number {port_in} invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if in_re_match.group(2):
            lane_in = int(in_re_match.group(2))
            if lane_in not in range(0, 4):
                raise CommandError(f"Invalid Ingress lane {lane_in}")

        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out and port_out <= 65):
                raise CommandError(f"Egress Port number {port_out} invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError(f"Invalid Egress lane {lane_out}")
        success, e_port = self.ports.get_dev_port(port_out, lane_out)
        success, i_port = self.ports.get_dev_port(port_in, lane_in)
        self.ptp.add_entry(i_port, e_port)

    def update_ptp_table_entry(self, in_re_match, out_re_match):
        """
        Update PTP Table entry. The PTP table match a given ingress port to an egress port for PTP traffic.
        This function creates an entry and take two arguments, first the ingress then egress port.
        This function aims for the traffic between Alveos to the PTP clock.
        """
        lane_in = None
        lane_out = None
        if in_re_match and in_re_match.group(1):
            port_in = int(in_re_match.group(1))
            if not (1 <= port_in and port_in <= 65):
                raise CommandError(f"Ingress Port number {port_in} invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if in_re_match.group(2):
            lane_in = int(in_re_match.group(2))
            if lane_in not in range(0, 4):
                raise CommandError(f"Invalid Ingress lane {lane_in}")

        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out and port_out <= 65):
                raise CommandError(f"Egress Port number {port_out} invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError(f"Invalid Egress lane {lane_out}")
        success, e_port = self.ports.get_dev_port(port_out, lane_out)
        success, i_port = self.ports.get_dev_port(port_in, lane_in)
        self.ptp.update_entry(i_port, e_port)

    def clear_ptp_table(self):
        """
        Remove all PTP table entries
        """
        self.ptp.clear()

    def configure_multicast_for_PTP(self, PTP_ports: list):
        """
        Adds PTP ports to the PTP multicast group

        :param PTP_ports: physical switch ports that connect to the Alveos
        """
        for port in PTP_ports:
            self.add_port_to_multicast_session(1, 1, port)

    # SDP related functions
    def add_sdp_ip_table_entry(self, ip_address: str, out_re_match):
        """Add an SDP IP forwarding Table entry. The SDP IP forwarding Table match a given IP address to an egress port.
        This function create a entry and take two arguments, the IP address and egress port.
        """
        lane_out = 0
        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out and port_out <= 65):
                raise CommandError("Egress Port number invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError("Invalid Egress lane")

        success, dev_port = self.ports.get_dev_port(port_out, lane_out)
        match = ip_address
        self.sdp_ip.add_entry(match, dev_port)

    def update_sdp_ip_table_entry(self, ip_address: str, out_re_match):
        """Updage an SDP IP forwarding Table entry.
        The SDP IP forwarding Table match a given IP address to an egress port.
        This function create a entry and take two arguments, the IP address and egress port.
        """
        lane_out = 0
        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out and port_out <= 65):
                raise CommandError("Egress Port number invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError("Invalid Egress lane")

        success, dev_port = self.ports.get_dev_port(port_out, lane_out)
        match = ip_address
        self.sdp_ip.update_entry(match, dev_port)

    def remove_sdp_ip_table_entry(self, ip_address: str):
        """Remove SDP IP forwarding Table entry.
        The SDP IP forwarding Table match a given IP address
        This function remove an entry
        """
        match = ip_address
        self.sdp_ip.remove_entry(match)

    def get_sdp_ip_table_counters(self):
        """Return counters of SDP IP forwarding Table."""
        entries = []
        try:
            for match, counters in self.sdp_ip.get_counters().items():
                entry = {
                    "IP_Address": "{}".format(match),
                    "Pkts": "{}".format(counters["Pkts"]),
                    "Bytes": "{}".format(counters["Bytes"]),
                }
                entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def get_sdp_ip_table_entries(self):
        """Return entries of SDP IP forwarding Table."""
        entries = []
        try:
            for match, dev_port in self.sdp_ip.get_entries().items():
                success2, fp_port, fp_lane = self.ports.get_fp_port(dev_port)
                if not success2:
                    raise CommandError("Port number invalid")
                else:
                    entry = {
                        "IP_Address": "{}".format(match),
                        "port": "{}/{}".format(fp_port, fp_lane),
                    }
                    entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def reset_sdp_ip_counters(self):
        """Reset counters of the SDP IP forwarding Table"""
        self.sdp_ip.reset_counters()

    def clear_sdp_ip_table(self):
        """Clear the SDP IP forwarding Table"""
        self.sdp_ip.clear()

    # SDP (MAC) related functions
    def add_sdp_mac_table_entry(self, ip_address: str, mac_address: str):
        """Add an SDP MAC forwarding Table entry.
        The SDP MAC forwarding Table match a given IP address to Dest Mac Address.
        This function create a entry and take two arguments, the IP address and MAC address.
        """
        self.sdp_mac.add_entry(ip_address, mac_address)

    def update_sdp_mac_table_entry(self, ip_address: str, mac_address: str):
        """Updage an SDP MAC forwarding Table entry.
        The SDP MAC forwarding Table match a given IP address to Dest Mac Address.
        This function create a entry and take two arguments, the IP address and MAC address.
        """
        self.sdp_mac.update_entry(ip_address, mac_address)

    def remove_sdp_mac_table_entry(self, ip_address: str):
        """Remove SDP MAC forwarding Table entry.
        The SDP MAC forwarding Table match a given IP address.
        This function remove an entry
        """
        match = ip_address
        self.sdp_mac.remove_entry(match)

    def get_sdp_mac_table_entries(self):
        """Return entries of PSR table."""
        entries = []
        try:
            for match, dev_port in self.sdp_mac.get_entries().items():
                entry = {
                    "IP_Address": "{}".format(match),
                    "MAC": "{}".format(dev_port),
                }
                entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def reset_sdp_mac_counters(self):
        """Reset counters of the PSR table"""
        self.sdp_mac.reset_counters()

    def clear_sdp_mac_table(self):
        """Clear the PSR table"""
        self.sdp_mac.clear()

    def get_health_status(self):
        """Getting a comprehensive dictionary with various health status of the switch

        We aim at reporting:
        * ports information:
          * Enable/Port_Up: True or False
          * Errors received: number
          * FCS errors received: number
          * Tx/Rx PPS: number
          * Tx/Rx Rate: number
        Advanced telemetry (To be fixed and exposed)

        :return: health status dictionary
        """
        health_status = {"ports": {}, "general": {}}
        success, ports = self.get_port_statistics()
        # self.log.info(ports)
        if success:
            for port in ports:
                port_name = port["$PORT_NAME"].strip()
                port_enable = port["$PORT_ENABLE"]
                port_up = port["$PORT_UP"]
                port_speed = port["$SPEED"]
                packet_received = port["packets_received"]
                packet_sent = port["packets_sent"]
                fcs_errors_received = port["FCS_errors_received"]
                errors_received = port["errors_received"]
                errors_sent = port["errors_sent"]
                tx_pps = port["TX_PPS"]
                rx_pps = port["RX_PPS"]
                tx_rate = port["TX_RATE"]
                rx_rate = port["RX_RATE"]

                health_status["ports"][port_name] = {
                    "Enable": port_enable,
                    "Up": port_up,
                    "Speed": port_speed,
                    "Rx": packet_received,
                    "Tx": packet_sent,
                    "FCS": fcs_errors_received,
                    "Rx_errors": errors_received,
                    "Tx_errors": errors_sent,
                    # TODO rate enabling in the port class
                    "TX_PPS": tx_pps,
                    "RX_PPS": rx_pps,
                    "TX_RATE": tx_rate,
                    "RX_RATE": rx_rate,
                }
        health_status["general"]["IP_address"] = self._ip_address
        health_status["general"]["Program"] = self._program_name
        health_status["general"]["TCP_port"] = self._tcp_port

        return health_status

    def add_sub_station_entry(self, sub_station: int):
        """
        Add a substation to be dropped by the switch.
        :param: sub_station the sub_station ID to drop
        """
        try:
            self.sub_station.entry_add(
                self.target,
                [
                    self.sub_station.make_key(
                        [
                            gc.KeyTuple("sub_station", sub_station),
                        ]
                    )
                ],
                [
                    self.sub_station.make_data(
                        [],
                        "drop",
                    )
                ],
            )
        except:
            self.log.info(
                f"Substation {sub_station} table entry already exists"
            )

    def remove_sub_station_entry(self, sub_station: int):
        """
        Remove a substation to be dropped by the switch. This will enable the sub_station again
        :param: sub_station the sub_station ID to no longer drop
        """
        try:
            self.sub_station.entry_del(
                self.target,
                [
                    self.sub_station.make_key(
                        [
                            gc.KeyTuple("sub_station", sub_station),
                        ]
                    )
                ],
            )
        except Exception as ex:
            self.log.info(f"Error when removing substation {sub_station}")

    def clear_sub_station_table(self):
        """
        Remove all entries from the sub_station table
        """
        entries_from_switch = self.sub_station.entry_get(self.target)
        for data, key in entries_from_switch:
            self.sub_station.entry_del(
                self.target,
                [
                    self.sub_station.make_key(
                        [
                            gc.KeyTuple(
                                "sub_station",
                                key.to_dict()["sub_station"]["value"],
                            ),
                        ]
                    )
                ],
            )

    def get_sub_station_table_entries(self):
        """
        Retrieve all entries from the sub_station table. This return a list of sub_station: id
        """
        entries_from_switch = self.sub_station.entry_get(self.target)
        entries = []
        for data, key in entries_from_switch:
            entries.append(
                {"sub_station": key.to_dict()["sub_station"]["value"]}
            )
        if len(entries) == 0:
            return False, entries
        return True, entries

    def add_port_mapping(self, board_num, port_num, lane_num):
        """
        Add port mapping manually.
        :param board_num: board number
        :param port_num: port number
        :param lane_num: lane number
        """
        self.ports.add_port_mapping(board_num, port_num, lane_num)
