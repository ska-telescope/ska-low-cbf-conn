import logging

from ska_low_cbf_net.connector_protocol import Protocol


class PTP(Protocol):
    def __init__(self, target, gc, bfrt_info):
        # Set up base class
        super(PTP, self).__init__(target, gc)
        self.table = bfrt_info.table_get("ptp_table")
        # self.multiplication = bfrt_info.table_get("multiplier_spead")

    def _clear(self):
        """Remove all entries"""
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            self.table.entry_del(
                self.target,
                [
                    self.table.make_key(
                        [
                            self.gc.KeyTuple(
                                "ingress_port",
                                key.to_dict()["ingress_port"]["value"],
                            )
                        ]
                    )
                ],
            )

    def clear(self):
        self._clear()

    def add_default_entries(self):
        """Add broadcast and default entries"""

    def add_entry(self, match, action):
        """Add one entry.

        Keyword arguments:
            action -- Dest port
            match -- Ingress port
        """

        self.table.entry_add(
            self.target,
            [self.table.make_key([self.gc.KeyTuple("ingress_port", match)])],
            [
                self.table.make_data(
                    [self.gc.DataTuple("dest_port", action)],
                    "set_egr_port_ptp",
                )
            ],
        )

    def add_entries(self, entry_list):
        """Add entries.

        Keyword arguments:
            entry_list -- a list of tuples: (ingress port, Dest port)
        """

        for match, action in entry_list:
            self.add_entry(match, action)

    def remove_entry(self, match):
        """Remove one entry"""
        self.table.entry_del(
            self.target,
            [self.table.make_key([self.gc.KeyTuple("ingress_port", match)])],
        )
        # del self.configure_ports[ingress_port]

    def get_match(self, action):
        """Get ingress port(s) for a given egress port.

        Returns:
            (success flag, ingress port(s) or error message)
        """
        entries = []
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if data.to_dict()["dest_port"] is action:
                entries.append(key.to_dict()["ingress_port"]["value"])
        if len(entries) == 0:
            return False, "Egress port not found"

        return True, entries

    def get_action(self, match):
        """Get egress port for a given ingress port.

        Returns:
            (success flag, egress port or error message)
        """
        try:
            results = self.table.entry_get(
                self.target,
                [
                    self.table.make_key(
                        [self.gc.KeyTuple("ingress_port", match)]
                    )
                ],
            )
            return results
        except:
            return None

    def get_entries(self):
        """Get all forwarding entries.

        Returns:
            list of (MAC address, dev port)
        """
        entries = {}
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            entries[key.to_dict()["ingress_port"]["value"]] = data.to_dict()[
                "dest_port"
            ]

        return entries

    def update_entry(self, match, action):
        """
        Keyword arguments:
            action -- Dest port
            match -- Ingress port
        """
        installed_action = False
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if key.to_dict()["ingress_port"]["value"] is match:
                installed_action = True
                break
        if installed_action:
            self.table.entry_mod(
                self.target,
                [
                    self.table.make_key(
                        [self.gc.KeyTuple("ingress_port", match)]
                    )
                ],
                [
                    self.table.make_data(
                        [self.gc.DataTuple("dest_port", action)],
                        "set_egr_port_ptp",
                    )
                ],
            )
        else:
            self.add_entry(match, action)
