Low CBF Connector
=================
"Connector" TANGO device server for the SKA-low Correlator and Beam-Former's In-Network Processors.

## Documentation
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-conn/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-conn/en/latest/?badge=latest)

The documentation for this project can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf-conn documentation](https://developer.skatelescope.org/projects/ska-low-cbf-conn/en/latest/index.html "SKA Developer Portal: ska-low-cbf-conn documentation")

## Project Avatar (Repository Icon)
[Connected icons created by Paul J. - Flaticon](https://www.flaticon.com/free-icons/connected "connected icons")

# Developer’s Guide

* The Makefiles here are inherited from [ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile).
  * Refer to docs at that repo, or use make help for details.
  * This link is via a git submodule, so use the --recursive flag when cloning this repository, or run git submodule update --init --recursive afterwards.


